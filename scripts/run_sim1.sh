#!/usr/bin/env bash

intexit() {
    # Kill all subprocesses (all processes in the current process group)
    kill -HUP -$$
}

hupexit() {
    # HUP'd (probably by intexit)
    echo
    echo "Interrupted"
    exit
}

trap hupexit HUP
trap intexit INT

# Measurment files
imu_meas=./meas/imu.meas
gps_meas=./meas/gps.meas

# Define pseudo-ports, remove them if they already exist
pty_in_imu=/tmp/in_imu.pty
pty_out_imu=/tmp/out_imu.pty
pty_in_gps=/tmp/in_gps.pty
pty_out_gps=/tmp/out_gps.pty

rm $pty_in_imu  2> /dev/null
rm $pty_out_imu 2> /dev/null
rm $pty_in_gps  2> /dev/null
rm $pty_out_gps 2> /dev/null

# Create connection between in & out pseudo-ports
socat PTY,link=$pty_in_imu,rawer PTY,link=$pty_out_imu,rawer &
socat PTY,link=$pty_in_gps,rawer PTY,link=$pty_out_gps,rawer &

# Run program
./SatIMU --imu_port $pty_out_imu --gnss_port $pty_out_gps &
#sleep 1
#cat $pty_out_imu 2> /dev/null &
#cat $pty_out_gps &

sleep 1
# Send data
i=1
# Read files and send to in pseudo-ports: TODO
i_max=$(wc -l < $imu_meas)
while [ $i -lt $i_max ]
do
	if [ $((i%100)) == 0 ]; then
		sed "$((i/100))q;d" $gps_meas > $pty_in_gps
	fi
	sed "$(echo $i)q;d" $imu_meas > $pty_in_imu

	((i++))
done

# Kill all background subprocesses & remove pseudo-ports
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT
echo "Done"

rm $pty_in_imu  2> /dev/null
rm $pty_out_imu 2> /dev/null
rm $pty_in_gps  2> /dev/null
rm $pty_out_gps 2> /dev/null
