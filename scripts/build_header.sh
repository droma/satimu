#! /usr/bin/env bash

# Check correct number of args
if [ ! $# -eq 1 ]
then
	echo -en "Usage:\n\t"
	echo "build_header.sh <path/to/file/header"
	exit 1
fi

# Gets name used include guards
header_path=$1
header_file=$(basename $header_path)
header_file_uc=$(echo $header_file | awk '{print toupper($0)}')
header_guards_name=_$(echo $header_file_uc | sed "s/\./_/g")_

if [ -f $header_path ]
then
	echo "Error: file already exists."
	exit 1
fi

if [ ! -d $(dirname $header_path) ]
then
	echo "Error: directory doesn't exist."
	exit 1
fi

touch $header_path
echo "#ifndef $header_guards_name" > $header_path
echo "#define $header_guards_name" >> $header_path
echo "" >> $header_path
echo "#endif" >> $header_path
