#! /bin/env bash


echo \[Copy\] src/*{.cpp,.h}
echo \[To\] ../backup/
cp src/*{.cpp,.h} ../backup/src

echo \[Copy\] ./mcpu
echo \[To\] ../backup/
cp -R mcpu ../backup/

