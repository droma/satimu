#! /usr/bin/env bash

folder=$1

make

#rm $folder/out.meas
rm $folder/out_ukf.meas
rm $folder/out_ekf.meas
rm $folder/out_aukf.meas
rm $folder/out_aekf.meas

#./SatIMU --file $folder
./SatIMU --file $folder --filter ekf
./SatIMU --file $folder --filter aekf
./SatIMU --file $folder --filter ukf
./SatIMU --file $folder --filter aukf
