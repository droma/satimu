#include <cstdio>
#include <array>
#include <iostream>
#include <string.h>

#include <sensors/file_mode.h>
#include <utils/algebra.h>
#include <wgs84.h>

int main(int argc, char* argv[])
{
	if (argc != 3) {
		perror("Two argument required:\nubx_to_nmea ubx_file nmea_file\n");
		return 1;
	}

	FILE* ubx = fopen(argv[1], "r");
	if (ubx == NULL) {
		perror("Can't open UBX file.");
		return 1;
	}

	FILE* nmea = fopen(argv[1], "w");
	if (ubx == NULL) {
		perror("Can't open UBX file.");
		return 1;
	}

	vector6d meas;
	vector3d llh;

	bool reading_files = true;
	while (reading_files) {

         //meas = read_ubx_nav_sol(ubx);
 		meas = get_gps_from_file(ubx);
		if ( feof(ubx) ) {
			continue;
			reading_files = false;
		}

		llh = ecef2llh( {{ meas[0], meas[1], meas[2] }} );
		//fprintf(nmea, "$GPGGA,0,%d,%c,%d,%c,2,4,%d,M,0,M,0000,0000*%c%c", )

	}

	fclose(ubx);
	fclose(nmea);

	return 0;
}
