#! /usr/bin/env bash

folder=$1

make

#rm $folder/out.meas
rm $folder/out_ukf.meas
rm $folder/out_ekf.meas

#./SatIMU --file $folder
./SatIMU --file $folder --filter ukf
./SatIMU --file $folder --filter ekf

matlab -nodisplay -nosplash -nodesktop -r "plot_sim(4); exit"
