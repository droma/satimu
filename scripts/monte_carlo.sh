#!/bin/bash

export MATLAB_JAVA=/usr/lib/jvm/java-8-openjdk/jre

for j in {1..100}
do
	matlab -nosplash -nodesktop -r "sim2 ; exit"
	./scripts/sim_all_filters.sh meas/sim2
	matlab -nosplash -nodesktop -r "plot_sim(2,0,0); exit"
done

