#include <grav_model.h>

vector3d grav_model_ned(vector3d llh)
{
	// Groves2008 p46

	vector3d grav;

	const scalar slat = sin(llh[0]);
	const scalar clat = cos(llh[0]);
	const scalar slat2 = slat*slat;
	const scalar clat2 = clat*clat;
	const scalar cslat = clat*slat;
	//const scalar clon = cos(llh[1]);
	//const scalar slon = sin(llh[1]);

	const scalar sqrt_1_less_e2slat2 = std::sqrt(1 - datum_e2*slat2);

	const scalar g0 = datum_gamma_e * (1 + datum_k*slat2) / sqrt_1_less_e2slat2;

	const scalar r_e = datum_a / sqrt_1_less_e2slat2;
	const scalar r_es2e = r_e * std::sqrt( clat2 + ( std::pow(1 - datum_e2, 2) ) * slat2);

	const vector3d u_D { { 0, 0, 1 } };
	vector3d gamma0;

	for (int i = 0; i < 3; ++i) {
		gamma0[i] = g0 * u_D[i];
	}

	// Gamma_0 = g0 * u_D + (O_ie^e ** 2) * r_es2s
	scalar datum_e_rot2 = datum_e_rot*datum_e_rot;
	gamma0[0] -= datum_e_rot2*r_es2e*( slat2*u_D[0] + cslat*u_D[2]);
	gamma0[1] -= datum_e_rot2*r_es2e*u_D[1];
	gamma0[2] -= datum_e_rot2*r_es2e*( cslat*u_D[0] + clat2*u_D[2]);

	const scalar height_factor = pow( r_es2e/(r_es2e + llh[2]), 2);
	for (int i = 0; i < 3; ++i) {
		gamma0[i] = height_factor*gamma0[i];
	}
	
	vector3d r_s = llh2ecef(llh);

	// Removes centrifugal force
	grav[0] = gamma0[0] - datum_e_rot2*r_s[0];
	grav[1] = gamma0[1] - datum_e_rot2*r_s[1];
	grav[2] = gamma0[2];

	//vector3d centri_f {{datum_e_rot2*r_s[0], datum_e_rot2*r_s[1], 0}};
	//_LOG_3D("Centrifugal force", centri_f);

	return grav;
}

vector3d grav_model_ecef(vector3d llh)
{
	// Groves2008 p46

	vector3d grav;

	const scalar slat = sin(llh[0]);
	const scalar clat = cos(llh[0]);
	const scalar slat2 = slat*slat;
	const scalar clat2 = clat*clat;
	const scalar clon = cos(llh[1]);
	const scalar slon = sin(llh[1]);

	//_LOG_V("slat2", slat2);
	const scalar sqrt_1_less_e2slat2 = std::sqrt(1 - datum_e2*slat2);
	//_LOG_V("Sqrt_1_less_e2slat2", sqrt_1_less_e2slat2);

	const scalar g0 = datum_gamma_e * (1 + datum_k*slat2) / sqrt_1_less_e2slat2;
	//_LOG_V("G0", g0);

	const scalar r_e = datum_a / sqrt_1_less_e2slat2;
	const scalar r_es2e = r_e * std::sqrt( clat2 + ( std::pow(1 - datum_e2, 2) ) * slat2);
	//_LOG_V("datum_a", datum_a);
	//_LOG_V("R_e", r_e);
	//_LOG_V("R_es2e", r_es2e);

	const vector3d u_D { {-clat*clon, -clat*slon, -slat } };
	//_LOG_3D("u_D", u_D);
	vector3d gamma0;

	for (int i = 0; i < 3; ++i) {
		gamma0[i] = g0 * u_D[i];
	}

	// Gamma_0 = g0 * u_D + (O_ie^e ** 2) * r_es2s
	scalar datum_e_rot2 = datum_e_rot*datum_e_rot;
	gamma0[0] -= datum_e_rot2*r_es2e*u_D[0];
	gamma0[1] -= datum_e_rot2*r_es2e*u_D[1];
	//_LOG_3D("Gamma0 no height factor", gamma0);

	const scalar height_factor = pow( r_es2e/(r_es2e + llh[2]), 2);
	for (int i = 0; i < 3; ++i) {
		gamma0[i] = height_factor*gamma0[i];
	}

	//_LOG_V("Height", llh[2]);
	//_LOG_V("Height factor", height_factor);
	//_LOG_3D("Gamma0", gamma0);
	
	vector3d r_s = llh2ecef(llh);

	// Removes centrifugal force
	grav[0] = gamma0[0] + datum_e_rot2*r_s[0];
	grav[1] = gamma0[1] + datum_e_rot2*r_s[1];
	grav[2] = gamma0[2];

	//vector3d centri_f {{datum_e_rot2*r_s[0], datum_e_rot2*r_s[1], 0}};
	//_LOG_3D("Centrifugal force", centri_f);

	return grav;
}
