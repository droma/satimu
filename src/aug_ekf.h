#ifndef __A_EKF_H__
#define __A_EKF_H__

#include <utils/algebra.h>
#include <utils/log.h>
#include <wgs84.h>
#include <nav_state.h>
#include <aug_kf_common.h>

Nav_State a_ekf(Nav_State curr, vector9d imu_meas, vector6d gnss_meas, scalar dt,
		bool new_meas, FILE* err_f);
void a_init_ekf();

#endif
