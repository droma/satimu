#include <nav_state.h>

void init_nav_state(Nav_State& nav_state)
{
	nav_state.att[0] = 1;

	for (int i = 0; i < 3; ++i) {
		nav_state.pos[i]   = 0;
		nav_state.vel[i]   = 0;
		nav_state.att[i+1] = 0;
	}
}
