#ifndef __KF_COMMON_H__
#define __KF_COMMON_H__

#include <utils/algebra.h>
#include <utils/log.h>
#include <wgs84.h>
#include <nav_state.h>

//#define _s_siz 10
#define _s_siz 10

#if _s_siz > 10
	#define _LOG_S _LOG_34D
#else
	#define _LOG_S _LOG_10D
#endif

const int s_siz   = _s_siz;
const int w_siz   = _s_siz-1;
const int m_siz   = 6;
const int sig_siz = 1 + s_siz*2;

using vector_s     = vector <s_siz>;
using vector_m     = vector <m_siz>;
using matrix_ss    = matrix <s_siz, s_siz>;
using matrix_sm    = matrix <s_siz, m_siz>;
using matrix_ms    = matrix <m_siz, s_siz>;
using matrix_mm    = matrix <m_siz, m_siz>;
using matrix_mm    = matrix <m_siz, m_siz>;
using matrix_ww    = matrix <w_siz, w_siz>;
using matrix_sw    = matrix <s_siz, w_siz>;
using matrix_sig_s = matrix <sig_siz, s_siz>;
using matrix_sig_m = matrix <sig_siz, m_siz>;

vector_s state_transition(vector_s s, vector3d pos, vector4d q_b2e,
		vector6d u, scalar dt);
vector_s reset_state();
Nav_State correct_nav_state(Nav_State curr, vector_s state);
void save_err_state(vector_s state, FILE* err_f);
vector_m get_kf_meas(vector6d gnss_meas, Nav_State ins_state);
matrix_ss init_cov();
matrix_ss init_q_disc();
//matrix_ww init_q();
matrix_mm init_r();
matrix_mm ekf_init_r();
matrix_sw get_g_k(vector4d q_b2e);
matrix_ss get_q_k(matrix_ww ekf_q_k, vector4d q_b2e, scalar dt);

#endif
