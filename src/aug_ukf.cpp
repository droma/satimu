#include <aug_ukf.h>

a_vector_s a_ukf_state;
a_matrix_ss a_cov;
a_matrix_ww a_q_k;
a_matrix_mm a_r_k;

void a_init_ukf()
{
	a_ukf_state = a_reset_state();

	a_cov = a_init_cov();
	//a_q_k = a_init_q();
	a_r_k = a_init_r();
}

a_vector_m a_predict_meas_from_state(a_vector_s s)
{
	a_vector_m z;

	z[0] = s[0];
	z[1] = s[1];
	z[2] = s[2];
	z[3] = s[3];
	z[4] = s[4];
	z[5] = s[5];

	return z;
}

a_matrix_sig_s a_get_sigma_points(a_matrix_ss cov, int n, scalar fact)
{
	a_matrix_sig_s sigma;

	sigma[0] = a_ukf_state;

	a_matrix_ss sqrt_cov = cholesky(a_cov);
	printf("SQRT(Cov)\n");
	print(sqrt_cov);

	scalar k = sqrt(fact);
	printf("K: %lf\n", k);
	//printf("a_ukf_state in get sigma points\n");
	//print(a_ukf_state);
	for (int i = 1; i <= n; ++i)
		for (int j = 0; j < n; ++j)
			sigma[i][j] = a_ukf_state[j] + k*sqrt_cov[i-1][j];

	for (int i = n+1; i < 2*n+1; ++i)
		for (int j = 0; j < n; ++j)
			sigma[i][j] = a_ukf_state[j] - k*sqrt_cov[i-n-1][j];

	return sigma;
}

struct a_weights {
	scalar m0, m, c0, c;
};

struct a_weights a_calculate_weights(scalar kappa, const int n)
{
	struct a_weights w;

	w.m0 = kappa/(n + kappa);
	w.m  = 0.5/(n + kappa);
	w.c0 = kappa/(n + kappa);
	w.c  = 0.5/(n + kappa);

	return w;
}

void a_ukf_iteration(Nav_State ns, vector6d u, a_vector_m y, scalar dt, bool new_meas)
{
	const int n   = a_s_siz;     // Size of state vector
	const int m   = a_m_siz;     // Size of measurments
	const int s_n = a_sig_siz;   // Number of sigma points
	a_matrix_sig_s sigma;        // Sigma points

	// Parameters
	//scalar kappa     = 0.7;
	//scalar kappa     = 0.2;
	scalar kappa     = 0;
	struct a_weights w = a_calculate_weights(kappa, n);

	sigma = a_get_sigma_points(a_cov, n, n+kappa);
	printf("Sigma\n");
	print(sigma);

	// Prediction
	a_matrix_sig_s sigma_pred;
	for (int i = 0; i < s_n; ++i)
		sigma_pred[i] = a_state_transition(sigma[i], ns.pos, ns.att, u, dt);

	printf("Sigma_pred = \n");
	print(sigma_pred);

	// Predict state
	a_vector_s x_pred;
	for (int i = 0; i < n; ++i) {
		x_pred[i] = w.m0*sigma_pred[0][i];
		for (int j = 1; j < s_n; ++j)
			x_pred[i] += w.m*sigma_pred[j][i];
	}

	printf("X_pred = \n");
	print(x_pred);

	if ( !new_meas ) {
		a_ukf_state = x_pred;
		return;
	}

	//a_matrix_ss q_now = get_q_k(q_k, ns.att, dt);
	a_matrix_ss q_now = a_init_q_disc();

	// Predict state covariance
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			a_cov[i][j] = q_now[i][j] + w.c0*(sigma_pred[0][i]
					- x_pred[i])*(sigma_pred[0][j] - x_pred[j]);
			for (int k = 1; k < s_n; ++k)
				a_cov[i][j] += w.c*(sigma_pred[k][i]
						- x_pred[i])*(sigma_pred[k][j] - x_pred[j]);
		}
	}

	// Get new sigma points
	sigma = a_get_sigma_points(a_cov, n, n+kappa);

	printf("S_y = \n");
	print(sigma);

	// Measurement prediction
	a_matrix_sig_m sigma_y;
	for (int i = 0; i < s_n; ++i)
		sigma_y[i] = a_predict_meas_from_state(sigma[i]);

	printf("S_pred = \n");
	print(sigma_y);

	a_vector_m y_pred;
	for (int i = 0; i < m; ++i) {
		y_pred[i] = w.m0*sigma_y[0][i];
		for (int j = 1; j < s_n; ++j)
			y_pred[i] += w.m*sigma_y[j][i];
	}

	//printf("Y_p = \n");
	//print(y_pred);
	//printf("Y = \n");
	//print(y);

	//a_matrix_mm cov_yy;
	//for (int i = 0; i < m; ++i) {
		//for (int j = 0; j < m; ++j) {
			//cov_yy[i][j] = r_k[i][j] + w.c0*(sigma_y[0][i]
					//- y_pred[i])*(sigma_y[0][j] - y_pred[j]);
			//for (int k = 1; k < s_n; ++k)
				//cov_yy[i][j] += w.c*(sigma_y[k][i]
						//- y_pred[i])*(sigma_y[k][j] - y_pred[j]);
		//}
	//}

	//a_matrix_mm cov_yy_I = inv_sim_pd(cov_yy);

	//a_matrix_sm cov_xy;
	//for (int i = 0; i < n; ++i) {
		//for (int j = 0; j < m; ++j) {
			//cov_xy[i][j] = w.c0*(sigma_pred[0][i] -
					//x_pred[i])*(sigma_y[0][j] - y_pred[j]);
			//for (int k = 1; k < s_n; ++k)
				//cov_xy[i][j] += w.c*(sigma_pred[k][i] -
						//x_pred[i])*(sigma_y[k][j] - y_pred[j]);
		//}
	//}
	//printf("Cov_yy\n");
	//print(cov_yy);
	//printf("Cov_yy^-1\n");
	//print(cov_yy_I);
	//printf("Cov_xy\n");
	//print(cov_xy);

	//// Kalman gain
	//a_matrix_sm gain = mult(cov_xy, cov_yy_I);
	//printf("K_gain\n");
	//print(gain);

	// Alternative for linear obs
	a_matrix_ms h = get_zero<a_m_siz, a_s_siz>();
	h[0][0] = h[1][1] = h[2][2] = h[3][3] = h[4][4] = h[5][5] = 1.0;

	a_matrix_mm s = mult(h, mult(a_cov,transpose(h)));
	s = add(s, a_r_k);
	s = inv_sim_pd(s);
	a_matrix_sm gain = mult(a_cov, transpose(h));
	gain = mult(gain, s);

	// State update
	a_vector_m y_res = sub(y, y_pred);
	for (int i = 0; i < n; ++i) {
		a_ukf_state[i] = x_pred[i];
		for (int j = 0; j < m; ++j)
			a_ukf_state[i] += gain[i][j]*y_res[j];
	}

	//printf("Y\n");
	//print(y);
	//printf("Y_pred\n");
	//print(y_pred);
	printf("Y_res\n");
	print(y_res);

	// Covariance update
	//a_matrix_sm dcov1 = mult(gain, cov_yy);
	//a_matrix_ms gain_t = transpose(gain);
	//a_matrix_ss dcov  = mult(dcov1, gain_t);
	//cov = sub(cov, dcov);
	//printf("K_gain*Cov_yy\n");
	//print(dcov1);
	//printf("K_gain^T\n");
	//print(gain_t);
	//printf("K_gain*Cov_yy*K_gain^T\n");
	//print(dcov);

	// Joseph form
	a_matrix_ss cov_fact = identity<a_s_siz>();
	cov_fact = sub(cov_fact, mult(gain, h));
	a_cov = mult(mult(cov_fact, a_cov), transpose(cov_fact));
	a_matrix_ss cov_r_term = mult(mult(gain, a_r_k), transpose(gain));
	a_cov = add(a_cov, cov_r_term);
	
	//a_matrix_ss id = identity<s_siz>();
	//a_matrix_ss dcov = sub(id, mult(gain, h));
	//cov = mult(dcov, cov);

	// Guarantee cov is symmetric and positive definite
	a_cov = force_symmetry(a_cov);

	printf("Cov\n");
	print(a_cov);

	// Check NAN in cov
	if ( check_nan(a_cov) )
		std::cin.get();
}

Nav_State a_ukf(Nav_State curr, vector9d imu_meas, vector6d gnss_meas, scalar dt,
		bool new_meas, FILE* err_f)
{
	_LOG("UKF");
	//scalar dt = 0.01;

	vector6d u {{ imu_meas[0], imu_meas[1], imu_meas[2], 
		          imu_meas[3], imu_meas[4], imu_meas[5] }};

	a_vector_m y = a_get_kf_meas(gnss_meas, curr);
	printf("GPS Meas residue\n");
	print(y);

	a_ukf_iteration(curr, u, y, dt, new_meas);
	printf("a_ukf_state\n");
	print(a_ukf_state);

	//check_constraints_v2(dt);

	curr = a_correct_nav_state(curr, a_ukf_state);
	if ( new_meas )
		a_save_err_state(a_ukf_state, err_f);

	// Reset state, so estimated error is not accounted for twice
	a_ukf_state = a_reset_state();

	return curr;
}
