#ifndef __ALIGNMENT_H__
#define __ALIGNMENT_H__

#include <iostream>
#include <fstream>

#include <sensors/sensors.h>
#include <sensors/file_mode.h>
#include <grav_model.h>
#include <nav_state.h>
#include <utils/algebra.h>
#include <utils/log.h>
#include <ukf.h>
#include <ekf.h>

void fine_alignment(FILE* imu_file, FILE* gps_file, FILE* real_file, 
		Nav_State& nav_state, Ports sensors, bool file_mode, 
		std::string filter, int freq);
void initialize(FILE* imu_file, FILE* gps_file, 
		FILE* real_file, Nav_State& nav_state, Ports sensors, 
		bool file_mode, int freq);

#endif
