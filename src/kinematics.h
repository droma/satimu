#ifndef __KINEMATICS_H__
#define __KINEMATICS_H__

#include <array>
#include <cmath>

#include <utils/log.h>
#include <nav_state.h>
#include <grav_model.h>
#include <wgs84.h>
#include <utils/algebra.h>

Nav_State kinematics(Nav_State& curr, vector9d& imu_meas, 
		vector6d gnss_meas, scalar dt);


#endif
