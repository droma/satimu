#ifndef __GRAV_MODEL_H__
#define __GRAV_MODEL_H__

#include <cmath>

#include <wgs84.h>
#include <utils/algebra.h>
#include <utils/log.h>

vector3d grav_model_ecef(vector3d llh);
vector3d grav_model_ned(vector3d llh);

#endif
