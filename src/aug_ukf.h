#ifndef __A_UKF_H__
#define __A_UKF_H__

#include <utils/algebra.h>
#include <utils/log.h>
#include <wgs84.h>
#include <nav_state.h>
#include <aug_kf_common.h>

//Nav_State ukf(Nav_State curr, vector9d imu_meas, vector6d gnss_meas);
Nav_State a_ukf(Nav_State curr, vector9d imu_meas, vector6d gnss_meas, scalar dt,
		bool new_meas, FILE* err_f);
void a_init_ukf();

#endif
