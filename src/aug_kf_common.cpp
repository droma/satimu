#include <aug_kf_common.h>

a_matrix_ss a_init_cov()
{
	a_matrix_ss cov;

	// for sim1 aug
	//scalar r_cov = 8e1;
	//scalar v_cov = 1e-1;
	//scalar q_cov = 7e-3;
	// for sim2 aug
	scalar r_cov = 1e1;
	scalar v_cov = 1e-1;
	scalar q_cov = 5e-4;
	// for sim aug
	//scalar r_cov = 1e4;
	//scalar v_cov = 1e2;
	//scalar q_cov = 1e0;
	// for exp
	//scalar r_cov = 1e4;
	//scalar v_cov = 1e2;
	//scalar q_cov = 1e0;

	scalar a_b_cov = 1e-3;
	scalar a_s_cov = 1e-3;
	scalar a_m_cov = 1e-3;
	scalar g_b_cov = 1e-3;
	scalar g_s_cov = 1e-3;
	scalar g_m_cov = 1e-3;

	cov = get_zero<a_s_siz, a_s_siz>();

	cov[0][0] = cov[1][1] = cov[2][2] = r_cov;
	cov[3][3] = cov[4][4] = cov[5][5] = v_cov;
	cov[6][6] = cov[7][7] = cov[8][8] = cov[9][9] = q_cov;

	if ( a_s_siz > 10 ) {
		cov[10][10] = cov[11][11] = cov[12][12] = a_b_cov;
		cov[13][13] = cov[14][14] = cov[15][15] = a_s_cov;
		cov[16][16] = cov[17][17] = cov[18][18] = cov[19][19] = cov[20][20] = cov[21][21] = a_m_cov;

		cov[22][22] = cov[23][23] = cov[24][24] = g_b_cov;
		cov[25][25] = cov[26][26] = cov[27][27] = g_s_cov;
		cov[28][28] = cov[29][29] = cov[30][30] = cov[31][31] = cov[32][32] = cov[33][33] = g_m_cov;
	}

	return cov;
}

a_matrix_ss a_ekf_init_q_disc()
{
	a_matrix_ss q_k;

	// sim1 ekf
	//scalar q_r   = 0;
	//scalar q_v   = 5e0;
	//scalar q_q   = 5e-1;
	// sim2
	scalar q_r   = 0;
	scalar q_v   = 5e0;
	scalar q_q   = 3e1;
	//scalar q_r   = 0;
	//scalar q_v   = 1.3e-2;
	//scalar q_q   = 4.4e-5;
	//scalar q_a_b = 1.32e-2;
	//scalar q_a_s = 1.32e-3;
	//scalar q_a_m = 1.32e-3;
	//scalar q_g_b = 1e-2;
	//scalar q_g_s = 1e-3;
	//scalar q_g_m = 1e-3;

	// sim1
	//scalar q_a_b = 1.32e-3;
	//scalar q_a_s = 1.32e-2;
	//scalar q_a_m = 1.32e-2;
	//scalar q_g_b = 4.4e-5;
	//scalar q_g_s = 4.4e-6;
	//scalar q_g_m = 4.4e-6;
	// sim2
	scalar q_a_b = 1.32e-3;
	scalar q_a_s = 1.32e-3;
	scalar q_a_m = 1.32e-4;
	scalar q_g_b = 1e-7;
	scalar q_g_s = 4.4e-5;
	scalar q_g_m = 4.4e-5;

	q_k = get_zero<a_s_siz, a_s_siz>();

	q_k[0][0] = q_k[1][1] = q_k[2][2] = q_r;
	q_k[3][3] = q_k[4][4] = q_k[5][5] = q_v;
	q_k[6][6] = q_k[7][7] = q_k[8][8] = q_k[9][9] = q_q;

	if ( a_s_siz > 10 ) {
		q_k[10][10] = q_k[11][11] = q_k[12][12] = q_a_b;
		q_k[13][13] = q_k[14][14] = q_k[15][15] = q_a_s;
		q_k[16][16] = q_k[17][17] = q_k[18][18] = q_k[19][19] = q_k[20][20] = q_k[21][21] = q_a_m;

		q_k[22][22] = q_k[23][23] = q_k[24][24] = q_g_b;
		q_k[25][25] = q_k[26][26] = q_k[27][27] = q_g_s;
		q_k[28][28] = q_k[29][29] = q_k[30][30] = q_k[31][31] = q_k[32][32] = q_k[33][33] = q_g_m;
	}

	return q_k;
}

a_matrix_ss a_init_q_disc()
{
	a_matrix_ss q_k;
	
	// sim1 ukf
	//scalar q_r   = 0;
	//scalar q_v   = 4e-1;
	//scalar q_q   = 1e-3;
	//scalar q_a_b = 1.32e-4;
	//scalar q_a_s = 1.32e-4;
	//scalar q_a_m = 1.32e-5;
	//scalar q_g_b = 4.4e-5;
	//scalar q_g_s = 4.4e-5;
	//scalar q_g_m = 4.4e-6;
	// sim2
	scalar q_r   = 0;
	scalar q_v   = 2e-2;
	scalar q_q   = 4.4e-7;
	scalar q_a_b = 1.32e-2;
	scalar q_a_s = 1.32e-3;
	scalar q_a_m = 1.32e-3;
	scalar q_g_b = 1e-2;
	scalar q_g_s = 1e-3;
	scalar q_g_m = 1e-3;

	// exp?
	//scalar q_r   = 0;
	//scalar q_v   = 1.3e0;
	//scalar q_q   = 4.4e-2;
	//scalar q_a_b = 1.32e-2;
	//scalar q_a_s = 1.32e-3;
	//scalar q_a_m = 1.32e-3;
	//scalar q_g_b = 4.4e-5;
	//scalar q_g_s = 4.4e-5;
	//scalar q_g_m = 4.4e-5;

	q_k = get_zero<a_s_siz, a_s_siz>();

	q_k[0][0] = q_k[1][1] = q_k[2][2] = q_r;
	q_k[3][3] = q_k[4][4] = q_k[5][5] = q_v;
	q_k[6][6] = q_k[7][7] = q_k[8][8] = q_k[9][9] = q_q;

	if ( a_s_siz > 10 ) {
		q_k[10][10] = q_k[11][11] = q_k[12][12] = q_a_b;
		q_k[13][13] = q_k[14][14] = q_k[15][15] = q_a_s;
		q_k[16][16] = q_k[17][17] = q_k[18][18] = q_k[19][19] = q_k[20][20] = q_k[21][21] = q_a_m;

		q_k[22][22] = q_k[23][23] = q_k[24][24] = q_g_b;
		q_k[25][25] = q_k[26][26] = q_k[27][27] = q_g_s;
		q_k[28][28] = q_k[29][29] = q_k[30][30] = q_k[31][31] = q_k[32][32] = q_k[33][33] = q_g_m;
	}

	return q_k;
}

a_matrix_mm a_ekf_init_r()
{
	a_matrix_mm r_k;

	// sim1 ekf
	//scalar r_r = 4e-1;
	//scalar r_v = 3.5e-1;
	// sim2 ekf
	scalar r_r = 1.4e0;
	scalar r_v = 4.2e-1;
	// for exp aug
	//scalar r_r = 5e-1;
	//scalar r_v = 9e-2;

	r_k = get_zero<a_m_siz, a_m_siz>();

	r_k[0][0] = r_k[1][1] = r_k[2][2] = r_r;
	r_k[3][3] = r_k[4][4] = r_k[5][5] = r_v;

	return r_k;
}

a_matrix_mm a_init_r()
{
	a_matrix_mm r_k;

	// sim1 ukf
	//scalar r_r = 2.8e0;
	//scalar r_v = 5e-1;
	// sim2 ukf
	scalar r_r = 4.1e0;
	scalar r_v = 4.25e-1;
	// for exp aug
	//scalar r_r = 1e1;
	//scalar r_v = 9e-1;

	r_k = get_zero<a_m_siz, a_m_siz>();

	r_k[0][0] = r_k[1][1] = r_k[2][2] = r_r;
	r_k[3][3] = r_k[4][4] = r_k[5][5] = r_v;

	return r_k;
}

a_vector_s a_state_transition(a_vector_s s, vector3d pos, vector4d q_b2e,
		vector6d u, scalar dt)
{
	a_vector_s ns = s; // Next state

	// Attitude
	vector3d c;
	vector4d q_e = conv_to_unit({{ s[6], s[7], s[8], s[9] }});

	const scalar q00 = q_e[0]*q_e[0];
	const scalar q11 = q_e[1]*q_e[1];
	const scalar q22 = q_e[2]*q_e[2];
	const scalar q33 = q_e[3]*q_e[3];
	const scalar q01 = q_e[0]*q_e[1];
	const scalar q02 = q_e[0]*q_e[2];
	const scalar q03 = q_e[0]*q_e[3];
	const scalar q12 = q_e[1]*q_e[2];
	const scalar q13 = q_e[1]*q_e[3];
	const scalar q23 = q_e[2]*q_e[3];

	vector3d w  = {{ u[3], u[4], u[5] }};
	if ( a_s_siz > 10 ) {
		vector3d nw;

		nw[0] = s[22] + s[25]*w[0] - s[28]*w[1] + s[29]*w[2];
		nw[1] = s[23] + s[26]*w[1] + s[30]*w[0] - s[31]*w[2];
		nw[2] = s[24] + s[27]*w[2] - s[32]*w[0] + s[33]*w[1];
		//nw = rot(nw, q_b2e);

		ns[6] += 0.5*dt*( -s[7]*nw[0] - s[8]*nw[1] - s[9]*nw[2] );
		ns[7] += 0.5*dt*(  s[6]*nw[0] - s[9]*nw[1] + s[8]*nw[2] );
		ns[8] += 0.5*dt*(  s[9]*nw[0] + s[6]*nw[1] - s[7]*nw[2] );
		ns[9] += 0.5*dt*( -s[8]*nw[0] + s[7]*nw[1] + s[6]*nw[2] );
	}

	// Attitude term
	// C = I - C(q)^-1
	c[0] =  (1-q00-q11+q22+q33)*w[0] - 2*(q03 + q12)*w[1] - 2*(q13 - q02)*w[2];
	c[1] = -2*(q12 - q03)*w[0] + (1-q00+q11-q22+q33)*w[1] - 2*(q01 + q23)*w[2];
	c[2] = -2*(q02 + q13)*w[0] - 2*(q23 - q01)*w[1] + (1-q00+q11+q22-q33)*w[2];

	// Multiply by dt*B
	ns[6] += 0.5*dt*( -q_e[1]*c[0] - q_e[2]*c[1] - q_e[3]*c[2] );
	ns[7] += 0.5*dt*(  q_e[0]*c[0] - q_e[3]*c[1] + q_e[2]*c[2] );
	ns[8] += 0.5*dt*(  q_e[3]*c[0] + q_e[0]*c[1] - q_e[1]*c[2] );
	ns[9] += 0.5*dt*( -q_e[2]*c[0] + q_e[1]*c[1] + q_e[0]*c[2] );

	vector4d q_unit = conv_to_unit( {{ ns[6], ns[7], ns[8], ns[9] }} );
	for (int i = 0; i < 4; ++i)
		ns[6+i] = q_unit[i];

	// Velocity
	// Gravity error term
	scalar pos_norm2 = pos[0]*pos[0] + pos[1]*pos[1] + pos[2]*pos[2];
	scalar grav_k    = datum_gm/pow(pos_norm2, 5);

	ns[3] -= dt*grav_k*( (pos_norm2 - 3*pos[0]*pos[0])*s[0] - 3*pos[0]*pos[1]*s[1] - 3*pos[0]*pos[2]*s[2] );
	ns[4] -= dt*grav_k*(-3*pos[0]*pos[1]*s[0] + (pos_norm2 - 3*pos[1]*pos[1])*s[1] - 3*pos[1]*pos[2]*s[2] );
	ns[5] -= dt*grav_k*(-3*pos[0]*pos[2]*s[0] - 3*pos[1]*pos[2]*s[1] + (pos_norm2 - 3*pos[2]*pos[2])*s[2] );

	// Centrifugal force
	scalar e_rot2 = datum_e_rot*datum_e_rot;
	ns[3] -= dt*e_rot2*s[0];
	ns[4] -= dt*e_rot2*s[1];
	ns[5] -= 0;

	// Earth rotation term
	ns[3] += dt*(  2*datum_e_rot*s[4] );
	ns[4] += dt*( -2*datum_e_rot*s[3] );
	ns[5] += dt*( 0 );

	// Sensor error term
	vector3d f  = {{ u[0], u[1], u[2] }};
	if ( a_s_siz > 10) {
		vector3d nf;
		nf[0] = s[10] + s[13]*f[0] - s[16]*f[1] + s[17]*f[2];
		nf[1] = s[11] + s[14]*f[1] + s[18]*f[0] - s[19]*f[2];
		nf[2] = s[12] + s[15]*f[2] - s[20]*f[0] + s[21]*f[1];
		nf = rot(nf, {{ s[6], -s[7], -s[8], -s[9] }} );
		nf = rot(nf, q_b2e);

		ns[3] += dt*nf[0];
		ns[4] += dt*nf[1];
		ns[5] += dt*nf[2];
	}

	// Orientation error term = dt*C_b^e*(I - C_c^b)f
	matrix3d dc = quat_to_dircos( {{ s[6], -s[7], -s[8], -s[9] }} );
	vector3d au;
	au[0] =  (1-dc[0][0])*f[0] - dc[0][1]*f[1] - dc[0][2]*f[2];
	au[1] = -dc[1][0]*f[0] + (1-dc[1][1])*f[1] - dc[1][2]*f[2];
	au[2] = -dc[2][0]*f[0] - dc[2][1]*f[1] + (1-dc[2][2])*f[2];
	vector3d dv_q = rot(au, q_b2e);

	ns[3] += dt*dv_q[0];
	ns[4] += dt*dv_q[1];
	ns[5] += dt*dv_q[2];

	// Position
	ns[0] += dt*s[3];
	ns[1] += dt*s[4];
	ns[2] += dt*s[5];

	// Sensor error
	if ( a_s_siz > 10 ) {
		for (int i = 10; i < a_s_siz; ++i) {
			ns[i] += dt*(0.01)*s[i];
		}
	}

	return ns;
}

a_vector_s a_reset_state()
{
	a_vector_s state;
	if ( a_s_siz == 10 )
		state = {{ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0 }};
	else {
		for (int i = 0; i < a_s_siz; ++i)
			state[i] = 0;
		state[6] = 1;
	}

	return state;
}

a_vector_m a_get_kf_meas(vector6d gnss_meas, Nav_State ins_state)
{
	a_vector_m meas_g;

	for (int i = 0; i < 3; ++i) {
		meas_g[i]   = gnss_meas[i]   - ins_state.pos[i];
		meas_g[i+3] = gnss_meas[i+3] - ins_state.vel[i];
	}

	return meas_g;
}

Nav_State a_correct_nav_state(Nav_State curr, a_vector_s state)
{
	curr.pos[0] += state[0];
	curr.pos[1] += state[1];
	curr.pos[2] += state[2];

	curr.vel[0] += state[3];
	curr.vel[1] += state[4];
	curr.vel[2] += state[5];

	vector4d q_corr {{ state[6], state[7], state[8], state[9] }};
	q_corr = conv_to_unit(q_corr);
	curr.att = mult(curr.att, q_corr);
	curr.att = conv_to_unit(curr.att);

	if (a_s_siz > 10) {
		bool plus_corr = 0;
		curr.acc_b[0] = plus_corr ? curr.acc_b[0] + state[10] : curr.acc_b[0] - state[10];
		curr.acc_b[1] = plus_corr ? curr.acc_b[1] + state[11] : curr.acc_b[1] - state[11];
		curr.acc_b[2] = plus_corr ? curr.acc_b[2] + state[12] : curr.acc_b[2] - state[12];
		curr.acc_s[0] = plus_corr ? curr.acc_s[0] + state[13] : curr.acc_s[0] - state[13];
		curr.acc_s[1] = plus_corr ? curr.acc_s[1] + state[14] : curr.acc_s[1] - state[14];
		curr.acc_s[2] = plus_corr ? curr.acc_s[2] + state[15] : curr.acc_s[2] - state[15];
		curr.acc_m[0] = plus_corr ? curr.acc_m[0] + state[16] : curr.acc_m[0] - state[16];
		curr.acc_m[1] = plus_corr ? curr.acc_m[1] + state[17] : curr.acc_m[1] - state[17];
		curr.acc_m[2] = plus_corr ? curr.acc_m[2] + state[18] : curr.acc_m[2] - state[18];
		curr.acc_m[3] = plus_corr ? curr.acc_m[3] + state[19] : curr.acc_m[3] - state[19];
		curr.acc_m[4] = plus_corr ? curr.acc_m[4] + state[20] : curr.acc_m[4] - state[20];
		curr.acc_m[5] = plus_corr ? curr.acc_m[5] + state[21] : curr.acc_m[5] - state[21];

		curr.gyro_b[0] = plus_corr ? curr.gyro_b[0] + state[22] : curr.gyro_b[0] - state[22];
		curr.gyro_b[1] = plus_corr ? curr.gyro_b[1] + state[23] : curr.gyro_b[1] - state[23];
		curr.gyro_b[2] = plus_corr ? curr.gyro_b[2] + state[24] : curr.gyro_b[2] - state[24];
		curr.gyro_s[0] = plus_corr ? curr.gyro_s[0] + state[25] : curr.gyro_s[0] - state[25];
		curr.gyro_s[1] = plus_corr ? curr.gyro_s[1] + state[26] : curr.gyro_s[1] - state[26];
		curr.gyro_s[2] = plus_corr ? curr.gyro_s[2] + state[27] : curr.gyro_s[2] - state[27];
		curr.gyro_m[0] = plus_corr ? curr.gyro_m[0] + state[28] : curr.gyro_m[0] - state[28];
		curr.gyro_m[1] = plus_corr ? curr.gyro_m[1] + state[29] : curr.gyro_m[1] - state[29];
		curr.gyro_m[2] = plus_corr ? curr.gyro_m[2] + state[30] : curr.gyro_m[2] - state[30];
		curr.gyro_m[3] = plus_corr ? curr.gyro_m[3] + state[31] : curr.gyro_m[3] - state[31];
		curr.gyro_m[4] = plus_corr ? curr.gyro_m[4] + state[32] : curr.gyro_m[4] - state[32];
		curr.gyro_m[5] = plus_corr ? curr.gyro_m[5] + state[33] : curr.gyro_m[5] - state[33];
		
		//curr.acc_b[0] += state[10];
		//curr.acc_b[1] += state[11];
		//curr.acc_b[2] += state[12];

		//curr.acc_s[0] -= state[13];
		//curr.acc_s[1] -= state[14];
		//curr.acc_s[2] -= state[15];

		//curr.acc_m[0] -= state[16];
		//curr.acc_m[1] -= state[17];
		//curr.acc_m[2] -= state[18];
		//curr.acc_m[3] -= state[19];
		//curr.acc_m[4] -= state[20];
		//curr.acc_m[5] -= state[21];

		//curr.gyro_b[0] += state[22];
		//curr.gyro_b[1] += state[23];
		//curr.gyro_b[2] += state[24];

		//curr.gyro_s[0] -= state[25];
		//curr.gyro_s[1] -= state[26];
		//curr.gyro_s[2] -= state[27];

		//curr.gyro_m[0] -= state[28];
		//curr.gyro_m[1] -= state[29];
		//curr.gyro_m[2] -= state[30];
		//curr.gyro_m[3] -= state[31];
		//curr.gyro_m[4] -= state[32];
		//curr.gyro_m[5] -= state[33];
	}

	return curr;
}

void a_save_err_state(a_vector_s state, FILE* err_f)
{
	fprintf(err_f, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf ", 
			state[0], state[1], state[2], state[3], state[4], state[5],
			state[6], state[7], state[8], state[9]);
	fprintf(err_f, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf ", 
			state[10], state[11], state[12], state[13], state[14], state[15],
			state[16], state[17], state[18], state[19], state[20], state[21]);
	fprintf(err_f, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", 
			state[22], state[23], state[24], state[25], state[26], state[27],
			state[28], state[29], state[30], state[31], state[32], state[33]);
}

a_matrix_sw a_get_g_k(vector4d q_b2e)
{
	a_matrix_sw g_k = get_zero<a_s_siz, a_w_siz>();

	matrix3d c_b2e = quat_to_dircos(q_b2e);
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			g_k[i+3][j+3] = -c_b2e[i][j];

	matrix4x3d b {{ {{ -0.5*q_b2e[1], -0.5*q_b2e[2], -0.5*q_b2e[3] }},
		            {{  0.5*q_b2e[0], -0.5*q_b2e[3],  0.5*q_b2e[2] }},
		            {{  0.5*q_b2e[3],  0.5*q_b2e[0], -0.5*q_b2e[1] }},
	                {{ -0.5*q_b2e[2],  0.5*q_b2e[1],  0.5*q_b2e[0] }} }};
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 3; ++j)
			g_k[i+6][j+6] = -b[i][j];

	printf("q_b2e\n");
	print(q_b2e);
	printf("C_b2e\n");
	print(c_b2e);
	printf("G_k\n");
	print(g_k);

	return g_k;
}

a_matrix_ss a_get_q_k(a_matrix_ww ekf_q_k, vector4d q_b2e, scalar dt)
{
	a_matrix_ss q_now;

	a_matrix_sw g_k = a_get_g_k(q_b2e);

	q_now = mult(mult(g_k, ekf_q_k), transpose(g_k));
	printf("Q_c\n");
	print(ekf_q_k);
	
	for (int i = 0; i < a_s_siz; i++)
		for (int j = 0; j < a_s_siz; j++)
			q_now[i][j] *= dt;
	printf("Q_k\n");
	print(q_now);
	//std::cin.get();

	return q_now;
}
