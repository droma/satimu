#include <utils/algebra.h>

vector4d mult(vector4d q, vector4d p)
{
	vector4d r;

	r[0] = q[0]*p[0] - q[1]*p[1] - q[2]*p[2] - q[3]*p[3];
	r[1] = q[0]*p[1] + q[1]*p[0] + q[2]*p[3] - q[3]*p[2];
	r[2] = q[0]*p[2] - q[1]*p[3] + q[2]*p[0] + q[3]*p[1];
	r[3] = q[0]*p[3] + q[1]*p[2] - q[2]*p[1] + q[3]*p[0];

	return r;
}

vector4d conv_to_unit(vector4d q)
{
	vector4d p;

	// Normalizing factor
	const scalar norm_f = sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);

	// Check norm = 0 singularity
	if (norm_f <= 0) {
		p = {{ 0, 0, 0, 0 }};
		return p;
	}

	p[0] = q[0]/norm_f;
	p[1] = q[1]/norm_f;
	p[2] = q[2]/norm_f;
	p[3] = q[3]/norm_f;

	return p;
}

vector4d conjugate(vector4d q)
{
	vector4d p;

	p[0] =  q[0];
	p[1] = -q[1];
	p[2] = -q[2];
	p[3] = -q[3];

	return p;
}

vector4d dircos_to_quat(matrix3d r)
{
	vector4d q;

	// Reference: http://www.ee.ucr.edu/~farrell/AidedNavigation/D_App_Quaternions/Rot2Quat.pdf
	// Reference: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/

	scalar tr = r[0][0] + r[1][1] + r[2][2];

	if (tr > 0 ) {
		scalar q0t2 = 2*sqrt(tr+1);
		//_LOG("1st try");

		q[0] = 0.25*q0t2;
		q[1] = (r[2][1] - r[1][2])/q0t2;
		q[2] = (r[0][2] - r[2][0])/q0t2;
		q[3] = (r[1][0] - r[0][1])/q0t2;
	} else if ( (r[0][0] > r[1][1]) && ( r[0][0] > r[2][2] ) ) {
		scalar q0t2 = 2*sqrt(1 + r[0][0] - r[1][1] - r[2][2]);
		//_LOG("2nd try");

		q[0] = (r[2][1] - r[1][2])/q0t2;
		q[1] = 0.25*q0t2;
		q[2] = (r[0][1] + r[1][0])/q0t2;
		q[3] = (r[0][2] + r[2][0])/q0t2;
	} else if ( r[1][1] > r[2][2] ) {
		scalar q0t2 = 2*sqrt(1 - r[0][0] + r[1][1] - r[2][2]);
		//_LOG("3rd try");

		q[0] = (r[0][2] - r[2][0])/q0t2;
		q[1] = (r[0][1] + r[1][0])/q0t2;
		q[2] = 0.25*q0t2;
		q[3] = (r[1][2] + r[2][1])/q0t2;
	} else {
		scalar q0t2 = 2*sqrt(1 - r[0][0] - r[1][1] + r[2][2]);
		//_LOG("4th try");

		q[0] = (r[1][0] - r[0][1])/q0t2;
		q[1] = (r[0][2] + r[2][0])/q0t2;
		q[2] = (r[1][2] + r[2][1])/q0t2;
		q[3] = 0.25*q0t2;
	}

	return q;
}

matrix3d quat_to_dircos(vector4d q)
{
	matrix3d d;

	scalar q00, q11, q22, q33;
	scalar q01, q02, q03, q12, q13, q23;

	q00 = q[0]*q[0];
	q11 = q[1]*q[1];
	q22 = q[2]*q[2];
	q33 = q[3]*q[3];
	q01 = q[0]*q[1];
	q02 = q[0]*q[2];
	q03 = q[0]*q[3];
	q12 = q[1]*q[2];
	q13 = q[1]*q[3];
	q23 = q[2]*q[3];

	d[0][0] = q00 + q11 - q22 - q33;
	d[0][1] = 2*(q12 - q03);
	d[0][2] = 2*(q13 + q02);
	d[1][0] = 2*(q12 + q03);
	d[1][1] = q00 - q11 + q22 - q33;
	d[1][2] = 2*(q23 - q01);
	d[2][0] = 2*(q13 - q02);
	d[2][1] = 2*(q23 + q01);
	d[2][2] = q00 - q11 - q22 + q33;

	return d;
}

vector3d rot(vector3d u, vector4d q)
{
	vector3d v;
	
	// Second order quaternion terms, ie. q00 = q0*q0, q01 = q0*q1, etc...
	scalar q00, q11, q22, q33;
	scalar q01, q02, q03, q12, q13, q23;

	q00 = q[0]*q[0];
	q11 = q[1]*q[1];
	q22 = q[2]*q[2];
	q33 = q[3]*q[3];
	q01 = q[0]*q[1];
	q02 = q[0]*q[2];
	q03 = q[0]*q[3];
	q12 = q[1]*q[2];
	q13 = q[1]*q[3];
	q23 = q[2]*q[3];

	//_LOG_3D("U", u);

	v[0] = (q00+q11-q22-q33)*u[0] + 2*(q12-q03)*u[1] + 2*(q13+q02)*u[2];
	v[1] = 2*(q12+q03)*u[0] + (q00-q11+q22-q33)*u[1] + 2*(q23-q01)*u[2];
	v[2] = 2*(q13-q02)*u[0] + 2*(q23+q01)*u[1] + (q00-q11-q22+q33)*u[2];

	//_LOG_3D("V", v);

	return v;
}

vector3d cross(vector3d u, vector3d v)
{
	vector3d w { { u[1]*v[2] - u[2]*v[1],
                   u[2]*v[0] - u[0]*v[2],
                   u[0]*v[1] - u[1]*v[0] } };

    return w;
}

scalar get_norm(vector3d v)
{
	scalar n;

	n = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);

	return n;
}

vector3d llh2ecef(vector3d llh)
{
	vector3d ecef;

	const scalar sin_lat = sin(llh[0]);
	const scalar sin_lat2 = sin_lat * sin_lat;
	const scalar cos_lat = cos(llh[0]);
	const scalar sin_lon = sin(llh[1]);
	const scalar cos_lon = cos(llh[1]);

	const scalar r_n = datum_a / sqrt(1 - datum_f*(2-datum_f)*sin_lat2);

	ecef[0] = (r_n + llh[2])*cos_lat*cos_lon;
	ecef[1] = (r_n + llh[2])*cos_lat*sin_lon;
	ecef[2] = (pow(1 - datum_f, 2)*r_n + llh[2])*sin_lat;

	return ecef;
}

vector3d ecef2llh(vector3d ecef)
{
	// Using Vermeille method and Bowring approximation (Fukushima Fast)
	vector3d llh;
	scalar p = sqrt(ecef[0]*ecef[0] + ecef[1]*ecef[1]);

	scalar fl = 1 - datum_f;
	scalar zl = fl*ecef[2];
	scalar al = datum_a*datum_e2;

	scalar t = ecef[2]/(fl*p);
	scalar c = 1/sqrt(1 + t*t);
	scalar s = c*t;
	t = (zl + al*s*s*s)/(p - al*c*c*c);

	llh[0] = atan2(t, fl);
	//_LOG_V("LAT", llh[0]);

	if (p > ecef[2])
		llh[2] = sqrt(1 + (t*t)/(fl*fl)) * (p - datum_a/sqrt(1+t*t));
	else
		llh[2] = sqrt(fl*fl + t*t)*(ecef[2]/t - datum_b/sqrt(1+t*t));
	//_LOG_V("HEI", llh[2]);

	//_LOG_V("PI/2", M_PI/2.0);
	//_LOG_V("Frac", ecef[0]/(p + ecef[1]));
	//_LOG_V("Delta", atan2(ecef[0], (sqrt(p) + ecef[1])));
	if (ecef[1] >= 0)
		llh[1] = M_PI/2.0 - 2*atan2(ecef[0], (p + ecef[1]));
	else
		llh[1] = -M_PI/2.0 + 2*atan2(ecef[0], (p - ecef[1]));
	//_LOG_V("LON", llh[1]);

	return llh;
}

matrix3d inv3d(matrix3d a)
{
	matrix3d b;

	scalar det = a[0][0] * (a[1][1] * a[2][2] - a[2][1] * a[1][2])
	           - a[0][1] * (a[1][0] * a[2][2] - a[1][2] * a[2][0])
	           + a[0][2] * (a[1][0] * a[2][1] - a[1][1] * a[2][0]);
	scalar invdet = 1/det;

	b[0][0] = (a[1][1] * a[2][2] - a[2][1] * a[1][2]) * invdet;
	b[0][1] = (a[0][2] * a[2][1] - a[0][1] * a[2][2]) * invdet;
	b[0][2] = (a[0][1] * a[1][2] - a[0][2] * a[1][1]) * invdet;
	b[1][0] = (a[1][2] * a[2][0] - a[1][0] * a[2][2]) * invdet;
	b[1][1] = (a[0][0] * a[2][2] - a[0][2] * a[2][0]) * invdet;
	b[1][2] = (a[1][0] * a[0][2] - a[0][0] * a[1][2]) * invdet;
	b[2][0] = (a[1][0] * a[2][1] - a[2][0] * a[1][1]) * invdet;
	b[2][1] = (a[2][0] * a[0][1] - a[0][0] * a[2][1]) * invdet;
	b[2][2] = (a[0][0] * a[1][1] - a[1][0] * a[0][1]) * invdet;

	return b;
}

matrix6d cholesky6d(matrix6d a)
{
	// Reference: https://rosettacode.org/wiki/Cholesky_decomposition#C
	matrix6d u;
	const int n = 6;

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			u[i][j] = 0;
		}
	}

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < (i+1); ++j) {
			scalar s = 0;
			for (int k = 0; k < j; ++k)
				s += u[i][k]*u[j][k];

			if (i == j) {
				u[i][j] = sqrt(a[i][i] - s);
			} else {
				u[i][j] = (a[i][j] - s)/u[j][j];
			}
		}
	}

	return u;
}

matrix6d inv6d_sim_pd(matrix6d a)
{
	matrix6d b;
	matrix6d lt = cholesky6d(a); // Lower triangle matrix
	const int n = 6;

	// Inversion of lt
	matrix6d lt_i;
	for (int i = 0; i < n; ++i) {
		lt_i[i][i] = 1/lt[i][i];
		
		for (int j = i+1; j < n; ++j)
			lt_i[i][j] = 0;
		
		for (int j = i-1; j >= 0; --j) {
			scalar sum = 0;
			for (int k = j; k < i; ++k)
				sum += lt[i][k]*lt_i[k][j];

			lt_i[i][j] = -sum/lt[i][i];
		}
	}
	// B = A^-1 = (L^-1)^T * L^-1;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			b[i][j] = 0;
			for (int k = 0; k < n; ++k)
				 b[i][j] += lt_i[k][i]*lt_i[k][j];
		}
	}

	return b;
}

matrix7d cholesky7d(matrix7d a)
{
	// Reference: https://rosettacode.org/wiki/Cholesky_decomposition#C
	matrix7d u;
	const int n = 7;

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			u[i][j] = 0;
		}
	}

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < (i+1); ++j) {
			scalar s = 0;
			for (int k = 0; k < j; ++k)
				s += u[i][k]*u[j][k];

			if (i == j) {
				u[i][j] = sqrt(a[i][i] - s);
			} else {
				u[i][j] = (a[i][j] - s)/u[j][j];
			}
		}
	}

	return u;
}

matrix7d inv7d_sim_pd(matrix7d a)
{
	matrix7d b;
	matrix7d lt = cholesky7d(a); // Lower triangle matrix
	matrix7d lt_i;
	const int n = 7;

	// Inversion of lt
	for (int i = 0; i < n; ++i) {
		lt_i[i][i] = 1/lt[i][i];
		
		for (int j = i+1; j < n; ++j)
			lt_i[i][j] = 0;
		
		for (int j = i-1; j >= 0; --j) {
			scalar sum = 0;
			for (int k = j; k < i; ++k)
				sum += lt[i][k]*lt_i[k][j];

			lt_i[i][j] = -sum/lt[i][i];
		}
	}
	// B = A^-1 = (L^-1)^T * L^-1;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			b[i][j] = 0;
			for (int k = 0; k < n; ++k)
				 b[i][j] += lt_i[k][i]*lt_i[k][j];
		}
	}

	return b;
}

matrix10d cholesky10d(matrix10d a)
{
	// Reference: https://rosettacode.org/wiki/Cholesky_decomposition#C
	matrix10d u;
	const int n = 10;

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			u[i][j] = 0;
		}
	}

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < (i+1); ++j) {
			scalar s = 0;
			for (int k = 0; k < j; ++k)
				s += u[i][k]*u[j][k];

			if (i == j) {
				u[i][j] = sqrt(a[i][i] - s);
			} else {
				u[i][j] = (a[i][j] - s)/u[j][j];
			}
		}
	}

	return u;
}

matrix34d cholesky34d(matrix34d a)
{
	// Reference: https://rosettacode.org/wiki/Cholesky_decomposition#C
	matrix34d u;
	const int n = 34;

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			u[i][j] = 0;
		}
	}

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < (i+1); ++j) {
			scalar s = 0;
			for (int k = 0; k < j; ++k)
				s += u[i][k]*u[j][k];

			if (i == j) {
				u[i][j] = sqrt(a[i][i] - s);
			} else {
				u[i][j] = (a[i][j] - s)/u[j][j];
			}
		}
	}

	return u;
}

