#ifndef __ALGEBRA_H__
#define __ALGEBRA_H__

#include <array>
#include <cmath>
#include <string>

#include <utils/log.h>
#include <wgs84.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#define __prec double

template <class T, std::size_t N>
struct s_array
{
	T data[N];

	T& operator[](std::size_t idx) { return data[idx]; }
	const T& operator[](std::size_t idx) const { return data[idx]; }
};

typedef __prec scalar;

template <size_t N, size_t M>
using matrix = s_array<s_array<__prec, M> ,N>;
template <size_t N>
using vector = s_array<__prec, N>;

typedef s_array<__prec,3>  vector3d;
typedef s_array<__prec,4>  vector4d;
typedef s_array<__prec,5>  vector5d;
typedef s_array<__prec,6>  vector6d;
typedef s_array<__prec,7>  vector7d;
typedef s_array<__prec,9>  vector9d;
typedef s_array<__prec,10> vector10d;
typedef s_array<__prec,34> vector34d;

typedef s_array<s_array<__prec,3> ,3>  matrix3d;
typedef s_array<s_array<__prec,4> ,3>  matrix3x4d;
typedef s_array<s_array<__prec,3> ,4>  matrix4x3d;
typedef s_array<s_array<__prec,4> ,4>  matrix4d;
typedef s_array<s_array<__prec,5> ,5>  matrix5d;
typedef s_array<s_array<__prec,6> ,6>  matrix6d;
typedef s_array<s_array<__prec,7> ,7>  matrix7d;
typedef s_array<s_array<__prec,9> ,9>  matrix9d;
typedef s_array<s_array<__prec,10>,10> matrix10d;
typedef s_array<s_array<__prec,3> ,10> matrix10x3d;
typedef s_array<s_array<__prec,6> ,10> matrix10x6d;
typedef s_array<s_array<__prec,34>,34> matrix34d;
typedef s_array<s_array<__prec,6> ,34> matrix34x6d;
typedef s_array<s_array<__prec,34> ,7> matrix7x34d;
typedef s_array<s_array<__prec,7> ,34> matrix34x7d;

// Quaternion
vector4d mult(vector4d q, vector4d p);
vector4d conjugate(vector4d q);
vector4d conv_to_unit(vector4d q);
vector4d dircos_to_quat(matrix3d r);
vector3d rot(vector3d u, vector4d q);
matrix3d quat_to_dircos(vector4d q);

// Frame convertions
vector3d llh2ecef(vector3d llh);
vector3d ecef2llh(vector3d ecef);

// General
vector3d  cross(vector3d u, vector3d v);
scalar    get_norm(vector3d v);
matrix3d  inv3d(matrix3d a);
matrix6d  inv6d_sim_pd(matrix6d a);
matrix7d  inv7d_sim_pd(matrix7d a);
matrix10d cholesky10d(matrix10d a);
matrix34d cholesky34d(matrix34d a);

// Templates
template <size_t N, size_t M>
matrix<N,M> get_zero()
{
	matrix<N,M> z;

	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			z[i][j] = 0;

	return z;
}

template <size_t N>
matrix<N,N> cholesky(matrix<N,N> a)
{
	// Reference: https://rosettacode.org/wiki/Cholesky_decomposition#C
	matrix<N,N> u;
	const int n = N;

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			u[i][j] = 0;
		}
	}

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < (i+1); ++j) {
			scalar s = 0;
			for (int k = 0; k < j; ++k)
				s += u[i][k]*u[j][k];

			if (i == j) {
				u[i][j] = sqrt(a[i][i] - s);
			} else {
				u[i][j] = (a[i][j] - s)/u[j][j];
			}
		}
	}

	return u;
}

template <size_t N>
matrix<N,N> inv_sim_pd(matrix<N,N> a)
{
	matrix<N,N> b;
	matrix<N,N> lt = cholesky(a); // Lower triangle matrix
	matrix<N,N> lt_i;
	const int n = N;

	// Inversion of lt
	for (int i = 0; i < n; ++i) {
		lt_i[i][i] = 1/lt[i][i];
		
		for (int j = i+1; j < n; ++j)
			lt_i[i][j] = 0;
		
		for (int j = i-1; j >= 0; --j) {
			scalar sum = 0;
			for (int k = j; k < i; ++k)
				sum += lt[i][k]*lt_i[k][j];

			lt_i[i][j] = -sum/lt[i][i];
		}
	}
	// B = A^-1 = (L^-1)^T * L^-1;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			b[i][j] = 0;
			for (int k = 0; k < n; ++k)
        b[i][j] += lt_i[k][i]*lt_i[k][j];
		}
	}

	return b;
}

template <size_t N, size_t M, size_t K>
matrix<N,M> mult(matrix<N,K> a, matrix<K,M> b)
{
	matrix<N,M> c;

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < M; ++j) {
			c[i][j] = 0;
			for (int k = 0; k < K; ++k)
				c[i][j] += a[i][k]*b[k][j];
		}

	return c;
}

template <size_t N, size_t M>
vector<N> mult(matrix<N,M> a, vector<M> b)
{
	vector<N> c;

	for (int i = 0; i < N; ++i) {
		c[i] = 0;
		for (int j = 0; j < M; ++j) {
			c[i] += a[i][j]*b[j];
		}
	}

	return c;
}

template <size_t N, size_t M>
matrix<N,M> add(matrix<N,M> a, matrix<N,M> b)
{
	matrix <N,M> c;

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < M; ++j)
			c[i][j] = a[i][j] + b[i][j];

	return c;
}

template <size_t N, size_t M>
matrix<N,M> sub(matrix<N,M> a, matrix<N,M> b)
{
	matrix <N,M> c;

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < M; ++j)
			c[i][j] = a[i][j] - b[i][j];

	return c;
}

template <size_t N>
vector<N> add(vector<N> a, vector<N> b)
{
	vector<N> c;

	for (int i = 0; i < N; ++i)
		c[i] = a[i] + b[i];

	return c;
}

template <size_t N>
vector<N> sub(vector<N> a, vector<N> b)
{
	vector<N> c;

	for (int i = 0; i < N; ++i)
		c[i] = a[i] - b[i];

	return c;
}

template <size_t N, size_t M>
matrix<N,M> transpose(matrix<M,N> a)
{
	matrix <N,M> b;

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < M; ++j)
			b[i][j] = a[j][i];

	return b;
}

template <size_t N, size_t M>
bool check_nan(matrix<N,M> a)
{
	for (int i = 0; i < N; ++i)
		for (int j = 0; j < M; ++j)
			if ( std::isnan(a[i][j]) )
				return true;

	return false;
}

template <size_t N>
matrix<N,N> force_symmetry(matrix<N,N> a)
{
	matrix<N,N> b;

	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < i; ++j) {
			b[i][j] = 0.5*(a[i][j] + a[j][i]);
			b[j][i] = b[i][j];
		}
		b[i][i] = a[i][i];
	}

	return b;
}

template <size_t N>
void print(vector<N> a)
{
	for (int j = 0; j < N; ++j)
		printf("%lf ", a[j]);
	printf("\n");
}

template <size_t N, size_t M>
void print(matrix<N,M> a)
{
	for (int i = 0; i < N; ++i) {
		printf("ln.%d: ", i);
		print(a[i]);
	}
}

template <size_t N>
matrix<N,N> identity()
{
	matrix<N,N> id = get_zero<N,N>();

	for (int i = 0; i < N; ++i)
		id[i][i] = 1;

	return id;
}

#endif
