#ifndef LOG_H__
#define LOG_H__

#include <iostream>
#include <iomanip>

#define _LOG(MESSAGE)                                                \
	std::cout << std::setprecision(10) << "[" << __FILE__ << ": " \
	          << __func__ << "] " << MESSAGE << "\n";
#define _LOG_V(MESSAGE, VAL) _LOG(MESSAGE << ": " << VAL)
#define _LOG_3D(MESSAGE, ARRAY) \
	_LOG_V(MESSAGE, ARRAY[0] << " " << ARRAY[1] << " " << ARRAY[2])
#define _LOG_4D(MESSAGE, ARRAY)                                               \
	_LOG_V(MESSAGE, ARRAY[0] << " " << ARRAY[1] << " " << ARRAY[2] << " " \
	                         << ARRAY[3])
#define _LOG_5D(MESSAGE, ARRAY)                                               \
	_LOG_V(MESSAGE, ARRAY[0] << " " << ARRAY[1] << " " << ARRAY[2] << " " \
	                         << ARRAY[3] << " " << ARRAY[4])
#define _LOG_6D(MESSAGE, ARRAY)                                               \
	_LOG_V(MESSAGE, ARRAY[0] << " " << ARRAY[1] << " " << ARRAY[2] << " " \
	                         << ARRAY[3] << " " << ARRAY[4] << " " << ARRAY[5])

#define _LOG_7D(MESSAGE, ARRAY)                                               \
	_LOG_V(MESSAGE, ARRAY[0] << " " << ARRAY[1] << " " << ARRAY[2] << " " \
	                         << ARRAY[3] << " " << ARRAY[4] << " "        \
	                         << ARRAY[5] << " " << ARRAY[6])
#define _LOG_9D(MESSAGE, ARRAY)                                              \
	_LOG_V(MESSAGE, ARRAY[0] << " " << ARRAY[1] << " " << ARRAY[2] << " " \
	                         << ARRAY[3] << " " << ARRAY[4] << " "        \
	                         << ARRAY[5] << " " << ARRAY[6] << " "        \
	                         << ARRAY[7] << " " << ARRAY[8])
#define _LOG_10D(MESSAGE, ARRAY)                                              \
	_LOG_V(MESSAGE, ARRAY[0] << " " << ARRAY[1] << " " << ARRAY[2] << " " \
	                         << ARRAY[3] << " " << ARRAY[4] << " "        \
	                         << ARRAY[5] << " " << ARRAY[6] << " "        \
	                         << ARRAY[7] << " " << ARRAY[8] << " "        \
	                         << ARRAY[9])
#define _LOG_34D(MESSAGE, ARRAY)                                              \
	_LOG_V(MESSAGE, ARRAY[0] << " " << ARRAY[1] << " " << ARRAY[2] << " " \
	                         << ARRAY[3] << " " << ARRAY[4] << " "        \
	                         << ARRAY[5] << " " << ARRAY[6] << " "        \
	                         << ARRAY[7] << " " << ARRAY[8] << " "        \
	                         << ARRAY[9] << " " << ARRAY[10] << " "        \
	                         << ARRAY[11] << " " << ARRAY[12] << " "        \
	                         << ARRAY[13] << " " << ARRAY[14] << " "        \
	                         << ARRAY[15] << " " << ARRAY[16] << " "        \
	                         << ARRAY[17] << " " << ARRAY[18] << " "        \
	                         << ARRAY[19] << " " << ARRAY[20] << " "        \
	                         << ARRAY[21] << " " << ARRAY[22] << " "        \
	                         << ARRAY[23] << " " << ARRAY[24] << " "        \
	                         << ARRAY[25] << " " << ARRAY[26] << " "        \
	                         << ARRAY[27] << " " << ARRAY[28] << " "        \
	                         << ARRAY[29] << " " << ARRAY[30] << " "        \
	                         << ARRAY[31] << " " << ARRAY[32] << " "        \
	                         << ARRAY[33])


#endif
