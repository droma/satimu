#include <options.h>

Options::Options(int argc, const char** argv)
{
	this->argc = argc;
	this->argv = argv;

	exe_path = argv[0];
}

void Options::parse()
{
	parse_config();
	parse_cmd_flags();
}

void Options::parse_config()
{
	std::string config_path;

	config_path = find_config_path();
	apply_config_opts(config_path);
}

// Find default config file in exe folder
std::string Options::find_config_path()
{
	std::string config_path;
	boost::filesystem::path exe_full_path, exe_dir;

	exe_full_path = boost::filesystem::system_complete(exe_path);
	exe_dir = exe_full_path.parent_path();
	
#ifndef _WIN32
	config_path = exe_dir.string() + "/conf.ini";
#elif 0
	config_path = exe_dir.parent_path().string() + "\\conf.ini";
#else
	config_path = exe_dir.string() + "\\conf.ini";
#endif

#ifdef _DEBUG
	_LOG_V("Config path", config_path);
#endif

	return config_path;
}

void Options::apply_config_opts(std::string config_path)
{
	Config_Parser config_parser(config_path);

	config_parser.get_prop(&time_opts.step_debug_mode, "Debug.Step_Debug_Mode");
	config_parser.get_prop(&time_opts.sample_freq,     "Main.Sample_Frequency");

	config_parser.get_prop(&sensor_opts.imu_port_path,  "Sensors.IMU_Port_Path");
	config_parser.get_prop(&sensor_opts.gnss_port_path, "Sensors.GNSS_Port_Path");

	config_parser.get_prop(&std_out_opts.verbose,        "Debug.Verbose");
	config_parser.get_prop(&std_out_opts.text_mode,      "Main.Text_Mode");
	config_parser.get_prop(&std_out_opts.display_in_enu, "Main.Display_In_ENU");
}

void Options::check_cmd_opt(std::unordered_map<const char*, const char*> cmd_opts, 
		bool& opt, const char* key)
{
	std::unordered_map<const char*, const char*>::const_iterator entry 
		= cmd_opts.find(key);
	if (entry != cmd_opts.end())
		opt = true;
}

void Options::check_cmd_opt(std::unordered_map<const char*, const char*> cmd_opts, 
		std::string& opt, const char* key)
{
	std::unordered_map<const char*, const char*>::const_iterator entry 
		= cmd_opts.find(key);
	if (entry != cmd_opts.end())
		opt = entry->second;
}

void Options::check_cmd_opt(std::unordered_map<const char*, const char*> cmd_opts, 
		int& opt, const char* key)
{
	std::unordered_map<const char*, const char*>::const_iterator entry 
		= cmd_opts.find(key);
	if (entry != cmd_opts.end())
		opt = atoi(entry->second);
}

void Options::parse_cmd_flags()
{
	const expected_opt opts[] = { {"file",           'f',  ARGUMENT_REQUIRED},
								  {"use_imu",         0,   NO_ARGUMENT_REQUIRED},
								  {"use_gnss",        0,   NO_ARGUMENT_REQUIRED},
								  {"imu_port",        0,   ARGUMENT_REQUIRED},
								  {"gnss_port",       0,   ARGUMENT_REQUIRED},
								  {"verbose",         'v', NO_ARGUMENT_REQUIRED},
								  {"text_mode",       't', NO_ARGUMENT_REQUIRED},
								  {"display_in_enu",  0,   NO_ARGUMENT_REQUIRED},
								  {"ignore_config",   0,   NO_ARGUMENT_REQUIRED},
								  {"config_path",     0,   ARGUMENT_REQUIRED},
								  {"step",            's', NO_ARGUMENT_REQUIRED},
								  {"filter",          0, ARGUMENT_REQUIRED},
								  {0, 0, 0} };

	std::unordered_map<const char*, const char*> cmd_opts;
	get_cmd_opts(argc, argv, opts, cmd_opts);

	check_cmd_opt(cmd_opts, sensor_opts.input_file_mode,  "file");
	check_cmd_opt(cmd_opts, sensor_opts.use_imu,          "use_imu");
	check_cmd_opt(cmd_opts, sensor_opts.use_gnss,         "use_gnss");
	check_cmd_opt(cmd_opts, sensor_opts.imu_port_path,    "imu_port");
	check_cmd_opt(cmd_opts, sensor_opts.gnss_port_path,   "gnss_port");

	check_cmd_opt(cmd_opts, std_out_opts.verbose,        "verbose");
	check_cmd_opt(cmd_opts, std_out_opts.text_mode,      "text_mode");
	check_cmd_opt(cmd_opts, std_out_opts.display_in_enu, "display_in_enu");

	check_cmd_opt(cmd_opts, time_opts.step_debug_mode, "step");
	check_cmd_opt(cmd_opts, kf_opts.filter,            "filter");
}

void Options::display()
{
	std::cout << "Sensor Options:" << std::endl
		<< "\tInput File Mode: " << sensor_opts.input_file_mode << std::endl
		<< "\tUse IMU: " << sensor_opts.use_imu << std::endl
		<< "\tUse GNSS: " << sensor_opts.use_gnss << std::endl
		<< "\tIMU Port Path: " << sensor_opts.imu_port_path
		<< std::endl
		<< "\tGNSS Port Path: " << sensor_opts.gnss_port_path
		<< std::endl;

	std::cout << "Standard Output Options:" << std::endl
		<< "\tVerbose: " << std_out_opts.verbose << std::endl
		<< "\tText Mode: " << std_out_opts.text_mode << std::endl
		<< "\tDisplay in ENU: " << std_out_opts.display_in_enu << std::endl;

}

bool Options::use_file()
{
	if (this->sensor_opts.input_file_mode.empty())
		return false;
	
	return true;
}
