#ifndef _OPTIONS_H_
#define _OPTIONS_H_

#include <string>
#include <iostream>
#include <cstdlib>
#include <unordered_map>

#include <boost/filesystem.hpp>

#include <opts/config_parser.h>
#include <opts/cmd_parser.h>
#include <utils/log.h>

typedef struct {
	std::string input_file_mode = "";

	bool use_imu = true;
	bool use_gnss = true;
	
	std::string imu_port_path = "/dev/null";
	std::string gnss_port_path = "/dev/null";
} Sensor_Options;

typedef struct {
	bool  step_debug_mode = false;
	int   sample_freq     = 100;
} Time_Options;

typedef struct {
	std::string filter = " ";
} KF_Options;

typedef struct {
	bool verbose = false;
	bool text_mode = false;
	bool display_in_enu = true;
} Std_Out_Options;

class Options
{
	private:

		int argc;
		const char** argv;
		const char* exe_path;

		void parse_config();
		//void parse_cmd_flags(CMD_Parser cmd_parser);
		void parse_cmd_flags();

		//bool config_is_to_be_parsed(CMD_Parser cmd_parser);

		std::string find_config_path();
		void apply_config_opts(std::string config_path);
		template <class T>
		T get_prop (ptree_t prop_tree, std::string prop);
		template <class T>
		T try_get_prop (ptree_t prop_tree, std::string prop);
		void check_cmd_opt(std::unordered_map<const char*, const char*> cmd_opts,
			bool& opt, const char* key);
		void check_cmd_opt(std::unordered_map<const char*, const char*> cmd_opts,
			std::string& opt, const char* key);
		void check_cmd_opt(std::unordered_map<const char*, const char*> cmd_opts,
			int& opt, const char* key);

	public:

		Time_Options time_opts;
		KF_Options kf_opts;
		Sensor_Options sensor_opts;
		Std_Out_Options std_out_opts;

		Options(int argc, const char** argv);

		void parse();
		void display();

		bool use_file();

};

#endif
