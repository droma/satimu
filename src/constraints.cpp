// Backup file

//void check_constraints_v2(scalar dt)
//{
	//const int n = 34;
	//const int m = 7;

	//vector7d d_vec {{ 1e3,
					  //10,
					  //1e-2,
					  //1,
					  //0.1,
					  //1e-3,
					  //1e-4 }};

	//matrix7x34d d;
	//for (int i = 0; i < m; ++i) {
		//for (int j = 0; j < n; ++j) {
			//d[i][j] = 0;
		//}
	//}
	//d[0][0]  = ukf_state[0];
	//d[0][1]  = ukf_state[1];
	//d[0][2]  = ukf_state[2];
	//d[1][3]  = ukf_state[3];
	//d[1][4]  = ukf_state[4];
	//d[1][5]  = ukf_state[5];
	//d[2][6]  = ukf_state[6];
	//d[2][7]  = ukf_state[7];
	//d[2][8]  = ukf_state[8];
	//d[2][9]  = ukf_state[9];
	//d[3][10] = ukf_state[10];
	//d[3][11] = ukf_state[11];
	//d[3][12] = ukf_state[12];
	//d[4][13] = ukf_state[13];
	//d[4][14] = ukf_state[14];
	//d[4][15] = ukf_state[15];
	//d[4][16] = ukf_state[16];
	//d[4][17] = ukf_state[17];
	//d[4][18] = ukf_state[18];
	//d[4][19] = ukf_state[19];
	//d[4][20] = ukf_state[20];
	//d[4][21] = ukf_state[21];
	//d[5][22] = ukf_state[22];
	//d[5][23] = ukf_state[23];
	//d[5][24] = ukf_state[24];
	//d[6][25] = ukf_state[25];
	//d[6][26] = ukf_state[26];
	//d[6][27] = ukf_state[27];
	//d[6][28] = ukf_state[28];
	//d[6][29] = ukf_state[29];
	//d[6][30] = ukf_state[30];
	//d[6][31] = ukf_state[31];
	//d[6][32] = ukf_state[32];
	//d[6][33] = ukf_state[33];

	//// F = D*x - d_vec;
	//vector7d f;
	//for (int i = 0; i < m; ++i) {
		//f[i] = -d_vec[i];
		//for (int j = 0; j < n; ++j) {
			//f[i] += d[i][j]*ukf_state[j];
		//}
	//}
	//_LOG_7D("F",f);

	//bool cont_flag = false;
	//for (int i = 0; i < m; ++i)
		//if (f[i] > 0) {
			//cont_flag = true;
			//break;
		//}
	//if (!cont_flag) return;

	//// A = P*D^T
	//matrix34x7d a;

	//for (int i = 0; i < n; ++i) {
		//for (int j = 0; j < m; ++j) {
			//a[i][j] = 0;
			//for (int k = 0; k < n; ++k) {
				//a[i][j] += cov[i][k]*d[j][k];
			//}
		//}
	//}
	//for (int i = 0; i < n; ++i) {
		//_LOG_7D("A", a[i]);
	//}

	//// B = D*P*D^T = D*A
	//matrix7d b;

	//for (int i = 0; i < m; ++i) {
		//for (int j = 0; j < m; ++j) {
			//b[i][j] = 0;
			//for (int k = 0; k < n; ++k) {
				//b[i][j] += d[i][k]*a[k][j];
			//}
		//}
	//}
	//for (int i = 0; i < m; ++i) {
		//_LOG_7D("B", b[i]);
	//}

	//// C = B^-1
	//matrix7d c = inv7d_sim_pd(b);
	//for (int i = 0; i < m; ++i) {
		//_LOG_7D("C", c[i]);
	//}

	//// E = A*C
	//matrix34x7d e;
	//for (int i = 0; i < n; ++i) {
		//for (int j = 0; j < m; ++j) {
			//e[i][j] = 0;
			//for (int k = 0; k < m; ++k) {
				//e[i][j] += a[i][k]*c[k][j];
			//}
		//}
	//}

	//// x+ = x- - E*F
	//for (int i = 0; i < n; ++i) {
		//for (int j = 0; j < m; ++j) {
			//ukf_state[i] -= e[i][j]*f[j];
		//}
	//}
//}

//void check_constraints(scalar dt)
//{
	//const int n = 34;
	//vector3d r_s {{ ukf_state[0], ukf_state[1], ukf_state[2] }};
	//scalar d_r = 20;

	//scalar r_norm2 = r_s[0]*r_s[0] + r_s[1]*r_s[1] + r_s[2]*r_s[2];
	//scalar d_r_res = r_norm2 - d_r*d_r;

	//if ( d_r_res > 0 ) {
		//_LOG("Contraint active!");
		//scalar k = d_r_res/(
				  //r_s[0]*(r_s[0]*cov[0][0]+r_s[1]*cov[1][0]+r_s[2]*cov[2][0])
				//+ r_s[1]*(r_s[0]*cov[0][1]+r_s[1]*cov[1][1]+r_s[2]*cov[2][1])
				//+ r_s[2]*(r_s[0]*cov[0][2]+r_s[1]*cov[1][2]+r_s[2]*cov[2][2]) );
		//for (int i = 0; i < n; ++i) {
			//ukf_state[i] -= k*(r_s[0]*cov[i][0] + r_s[1]*cov[i][1] + r_s[2]*cov[i][2]);
		//}
		//_LOG_34D("CUKF_State", ukf_state);

		//matrix_ss miu;
		//scalar miu_k = 1/(
			  //r_s[0]*(r_s[0]*cov[0][0]+r_s[1]*cov[1][0]+r_s[2]*cov[2][0])
			//+ r_s[1]*(r_s[0]*cov[0][1]+r_s[1]*cov[1][1]+r_s[2]*cov[2][1])
			//+ r_s[2]*(r_s[0]*cov[0][2]+r_s[1]*cov[1][2]+r_s[2]*cov[2][2]) );
		//for (int i = 0; i < n; ++i) {

			//miu[i][0] = -miu_k*(cov[i][0]*r_s[0]*r_s[0] + cov[i][1]*r_s[0]*r_s[1] + cov[i][2]*r_s[0]*r_s[2]);
			//miu[i][1] = -miu_k*(cov[i][0]*r_s[0]*r_s[1] + cov[i][1]*r_s[1]*r_s[1] + cov[i][2]*r_s[1]*r_s[2]);
			//miu[i][2] = -miu_k*(cov[i][0]*r_s[0]*r_s[2] + cov[i][1]*r_s[1]*r_s[1] + cov[i][2]*r_s[2]*r_s[2]);
			//for (int j = 3; j < n; ++j) {
				//miu[i][j] = 0;
			//}
			//miu[i][i] += 1;
		//}
		
		//matrix_ss cov_c = cov;
		//for (int i = 0; i < n; ++i) {
			//for (int j = 0; j < n; ++j) {
				//cov_c[i][j] = 0;
				//for (int k = 0; k < n; ++k) {
					//cov_c[i][j] += miu[i][k]*cov[k][j];
				//}
			//}
		//}
		//for (int i = 0; i < n; ++i) {
			//for (int j = 0; j < n; ++j) {
				//cov[i][j] = 0;
				//for (int k = 0; k < n; ++k) {
					//cov[i][j] += cov_c[i][k]*miu[j][k];
				//}
			//}
		//}
		//for (int i = 0; i < n; ++i)
			//_LOG_34D("CCov", cov[i]);
		
		////std::cin.get();
	//}
//}
