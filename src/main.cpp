#include <array>
#include <cstdio>
#include <iomanip>
#include <string>

#if WIN32
	#include <io.h>
#else
	#include <unistd.h>
#endif

#include <alignment.h>
#include <aug_ekf.h>
#include <aug_ukf.h>
#include <ekf.h>
#include <ukf.h>
#include <kinematics.h>
#include <nav_state.h>
#include <options.h>
#include <sensors/file_mode.h>
#include <sensors/sensors.h>
#include <utils/algebra.h>
#include <utils/log.h>

void print_kin(Nav_State ns)
{
	printf("Pos = %lf, %lf, %lf\n", ns.pos[0], ns.pos[1], ns.pos[2]);
	printf("Vel = %lf, %lf, %lf\n", ns.vel[0], ns.vel[1], ns.vel[2]);
	printf("q_b2e = %lf, %lf, %lf, %lf\n", ns.att[0], ns.att[1], ns.att[2],
	       ns.att[3]);

	printf("Acc_b = %lf, %lf, %lf\n", ns.acc_b[0], ns.acc_b[1],
	       ns.acc_b[2]);
	printf("Acc_s = %lf, %lf, %lf\n", ns.acc_s[0], ns.acc_s[1],
	       ns.acc_s[2]);
	printf("Acc_m = %lf, %lf, %lf, %lf, %lf, %lf \n", ns.acc_m[0],
	       ns.acc_m[1], ns.acc_m[2], ns.acc_m[3], ns.acc_m[4], ns.acc_m[5]);

	printf("Gyro_b = %lf, %lf, %lf\n", ns.gyro_b[0], ns.gyro_b[1],
	       ns.gyro_b[2]);
	printf("Gyro_s = %lf, %lf, %lf\n", ns.gyro_s[0], ns.gyro_s[1],
	       ns.gyro_s[2]);
	printf("Gyro_m = %lf, %lf, %lf, %lf, %lf, %lf \n", ns.gyro_m[0],
	       ns.gyro_m[1], ns.gyro_m[2], ns.gyro_m[3], ns.gyro_m[4],
	       ns.gyro_m[5]);

	_LOG_V("|V|", get_norm(ns.vel));
}

void print_imu(vector9d meas)
{
	printf("imu_meas = (%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf)\n", meas[0],
	       meas[1], meas[2], meas[3], meas[4], meas[5], meas[6], meas[7],
	       meas[8]);
}

void print_gps(vector6d meas)
{
	printf("gps_meas = (%lf,%lf,%lf,%lf,%lf,%lf)\n", meas[0], meas[1],
	       meas[2], meas[3], meas[4], meas[5]);
}

int main(int argc, const char** argv)
{
	// Parse options and save in Options class
	Options* app_options = new Options(argc, argv);
	app_options->parse();

	const int    freq = app_options->time_opts.sample_freq;
	const scalar dt   = 1.0f / (int)freq;

	vector9d  imu_meas;
	vector6d  gnss_meas;
	Nav_State nav_state;

	// Setup input files and or sensors
	FILE *real_f = NULL, *imu_f = NULL, *gps_f = NULL, *out_f = NULL,
	     *err_f = NULL;
	Ports sensors;
	if (app_options->use_file()) {
		std::string file_folder =
		        app_options->sensor_opts.input_file_mode;
		if (setup_files(file_folder, &imu_f, &gps_f, &real_f, &out_f,
		                &err_f, app_options->kf_opts.filter))
			return 1;
	} else {
		sensors = sensor_setup(app_options->sensor_opts);
		if (sensors.imu == -1 || sensors.gnss == -1) {
			printf("Can't use imu or gnss port\n");
			printf("Exiting...\n");
			return 1;
		}
	}

	init_ukf();
	init_ekf();
	a_init_ukf();
	a_init_ekf();
	initialize(imu_f, gps_f, real_f, nav_state, sensors,
	           app_options->use_file(), freq);
	// fine_alignment(imu_f, gps_f, real_f, nav_state, sensors,
	// app_options->use_file(), app_options->kf_opts.filter, freq);

	int i = 0;
	printf("Running algorithm\n");
	while (1) {
		bool gps_invalid = false;

		// printf("\n\nIteration no. %d\n", i);
		if (app_options->use_file()) {
			imu_meas = get_imu_from_file(imu_f);
			print_imu(imu_meas);

			if (i % freq == 0) {
				gnss_meas = get_gps_from_file(gps_f);
				print_gps(gnss_meas);
				// std::cin.get();
				if (get_norm({{gnss_meas[0], gnss_meas[1],
				               gnss_meas[2]}}) < 100) {
					gps_invalid = true;
					printf("Invalid GPS\n");
					// std::cin.get();
				}
			}

			// if ( feof(imu_f) || feof(real_f) || feof(gps_f) ) {
			if (feof(imu_f) || feof(gps_f)) {
				_LOG("EOF... breaking...")
				break;
			}
		} else {
			imu_meas = parse_imu(sensors.imu);
			print_imu(imu_meas);

			if (i % freq == 0) {
				gnss_meas = parse_gnss(sensors.gnss);
				print_gps(gnss_meas);
			}
		}

		nav_state = kinematics(nav_state, imu_meas, gnss_meas, dt);

		if (!gps_invalid) {
			int f_ukf = 1;
			if (app_options->kf_opts.filter.compare("ukf") == 0 &&
			    i % (freq / f_ukf) == 0)
				nav_state = ukf(nav_state, imu_meas, gnss_meas,
				                (scalar)1 / f_ukf, i % 100 == 0,
				                err_f);
			int f_aukf = 1;
			if (app_options->kf_opts.filter.compare("aukf") == 0 &&
			    i % (freq / f_aukf) == 0)
				nav_state = a_ukf(nav_state, imu_meas,
				                  gnss_meas, (scalar)1 / f_aukf,
				                  i % 100 == 0, err_f);

			// freq of predicion
			int f_ekf = 1;
			if (app_options->kf_opts.filter.compare("ekf") == 0 &&
			    i % (freq / f_ekf) == 0)
				nav_state = ekf(nav_state, imu_meas, gnss_meas,
				                (scalar)1 / f_ekf, i % 100 == 0,
				                err_f);
			int f_aekf = 1;
			if (app_options->kf_opts.filter.compare("aekf") == 0 &&
			    i % (freq / f_aekf) == 0)
				nav_state = a_ekf(nav_state, imu_meas,
				                  gnss_meas, (scalar)1 / f_aekf,
				                  i % 100 == 0, err_f);
		}

		// Present results
		print_kin(nav_state);

		if (app_options->use_file())
			compare_estimate_to_reality(nav_state, real_f, out_f);

		if (app_options->time_opts.step_debug_mode)
			if (i % 100 == 0) std::cin.get();

		i++;
	}

	// Cleanup
	delete app_options;

	// Close files
	print_rmse();
	if (app_options->use_file())
		close_files(&imu_f, &gps_f, &real_f, &out_f, &err_f);
}
