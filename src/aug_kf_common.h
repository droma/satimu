#ifndef __AKF_COMMON_H__
#define __AKF_COMMON_H__

#include <utils/algebra.h>
#include <utils/log.h>
#include <wgs84.h>
#include <nav_state.h>

//#define _s_siz 10
#define _a_s_siz 34

//#if __s_siz > 10
	//#define _LOG_S _LOG_34D
//#else
	//#define _LOG_S _LOG_10D
//#endif

const int a_s_siz   = _a_s_siz;
const int a_w_siz   = _a_s_siz-1;
const int a_m_siz   = 6;
const int a_sig_siz = 1 + a_s_siz*2;

using a_vector_s     = vector <a_s_siz>;
using a_vector_m     = vector <a_m_siz>;
using a_matrix_ss    = matrix <a_s_siz, a_s_siz>;
using a_matrix_sm    = matrix <a_s_siz, a_m_siz>;
using a_matrix_ms    = matrix <a_m_siz, a_s_siz>;
using a_matrix_mm    = matrix <a_m_siz, a_m_siz>;
using a_matrix_mm    = matrix <a_m_siz, a_m_siz>;
using a_matrix_ww    = matrix <a_w_siz, a_w_siz>;
using a_matrix_sw    = matrix <a_s_siz, a_w_siz>;
using a_matrix_sig_s = matrix <a_sig_siz, a_s_siz>;
using a_matrix_sig_m = matrix <a_sig_siz, a_m_siz>;

a_vector_s a_state_transition(a_vector_s s, vector3d pos, vector4d q_b2e,
		vector6d u, scalar dt);
a_vector_s a_reset_state();
Nav_State a_correct_nav_state(Nav_State curr, a_vector_s state);
void a_save_err_state(a_vector_s state, FILE* err_f);
a_vector_m a_get_kf_meas(vector6d gnss_meas, Nav_State ins_state);
a_matrix_ss a_init_cov();
a_matrix_ss a_init_q_disc();
a_matrix_ss a_ekf_init_q_disc();
a_matrix_ww a_init_q();
a_matrix_mm a_init_r();
a_matrix_mm a_ekf_init_r();
a_matrix_sw a_get_g_k(vector4d q_b2e);
a_matrix_ss a_get_q_k(a_matrix_ww ekf_q_k, vector4d q_b2e, scalar dt);

#endif
