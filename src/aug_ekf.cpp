#include <aug_ekf.h>

a_vector_s  a_ekf_state;
a_matrix_ss a_ekf_cov;
a_matrix_ww a_ekf_q_k;
a_matrix_mm a_ekf_r_k;

void a_init_ekf()
{
	a_ekf_state = a_reset_state();

	a_ekf_cov = a_init_cov();
	//ekf_q_k = init_q_disc();
	a_ekf_r_k = a_ekf_init_r();
}

a_matrix_ss a_get_f_ekf(vector3d pos, vector4d q_b2e, vector6d u, scalar dt)
{
	//a_matrix_ss f = get_zero<s_siz, s_siz>();
	a_matrix_ss f = identity<a_s_siz>();
	//printf("init f\n");
	//print(f);

	// Position eq - velocity term
	f[0][3] += dt;
	f[1][4] += dt;
	f[2][5] += dt;

	// Velocity eq - gravity/position term
	scalar pos_norm2 = pos[0]*pos[0] + pos[1]*pos[1] + pos[2]*pos[2];
	scalar grav_k    = datum_gm/pow(pos_norm2, 5);

	f[3][0] += dt*grav_k*(pos_norm2 - 3*pos[0]*pos[0]);
	f[3][1] += dt*grav_k*( -3*pos[0]*pos[1]);
	f[3][2] += dt*grav_k*( -3*pos[0]*pos[2]);
	f[4][0] += dt*grav_k*( -3*pos[1]*pos[0]);
	f[4][1] += dt*grav_k*(pos_norm2 - 3*pos[1]*pos[1]);
	f[4][2] += dt*grav_k*( -3*pos[1]*pos[2]);
	f[5][0] += dt*grav_k*( -3*pos[2]*pos[0]);
	f[5][1] += dt*grav_k*( -3*pos[2]*pos[1]);
	f[5][2] += dt*grav_k*(pos_norm2 - 3*pos[2]*pos[2]);

	// Velocity eq - earth rotation/velocity term
	f[3][4] += dt*2*datum_e_rot;
	f[4][3] -= dt*2*datum_e_rot;
	
	// Velocity eq - attitude term

	vector3d a {{ u[0], u[1], u[2] }};
	//scalar f0 = 2*(-a[0] + a[1] - a[2]);
	//scalar f1 = 2*(-a[0] - a[1] - a[2]);
	//scalar f2 = 2*( a[0] - a[1] - a[2]);
	//scalar f3 = 2*( a[0] + a[1] - a[2]);
	//scalar f4 = 2*(-a[0] - a[1] + a[2]);
	//scalar f5 = 2*(-a[0] + a[1] + a[2]);
	//scalar f6 = 2*( a[0] - a[1] + a[2]);
	scalar f0 = 2*(-a[0] - a[1] - a[2]);
	scalar f1 = 2*(-a[0] - a[1] + a[2]);
	scalar f2 = 2*(-a[0] + a[1] - a[2]);
	scalar f3 = 2*(-a[0] + a[1] + a[2]);
	scalar f4 = 2*( a[0] - a[1] - a[2]);
	scalar f5 = 2*( a[0] - a[1] + a[2]);
	scalar f6 = 2*( a[0] + a[1] - a[2]);

	//matrix3x4d f_mat {{ {{ f0, f1, f2, f3 }},
						//{{ f4, f5, f1, f0 }},
						//{{ f2, f4, f6, f1 }} }};
	matrix3x4d f_mat {{ {{ f1, f0, f5, f4 }},
						{{ f4, f2, f0, f6 }},
						{{ f2, f3, f1, f0 }} }};

	matrix3d c_b2e = quat_to_dircos(q_b2e);
	f_mat = mult(c_b2e, f_mat);

	//f_mat = mult(c_b2e, f_mat);
	f[3][6] += dt*f_mat[0][0];
	f[3][7] += dt*f_mat[0][1];
	f[3][8] += dt*f_mat[0][2];
	f[3][9] += dt*f_mat[0][3];
	f[4][6] += dt*f_mat[1][0];
	f[4][7] += dt*f_mat[1][1];
	f[4][8] += dt*f_mat[1][2];
	f[4][9] += dt*f_mat[1][3];
	f[5][6] += dt*f_mat[2][0];
	f[5][7] += dt*f_mat[2][1];
	f[5][8] += dt*f_mat[2][2];
	f[5][9] += dt*f_mat[2][3];

	// Velocity eq - acc term
	matrix<3,12> f_va = get_zero<3,12>();
	f_va[0][0] = f_va[1][1] = f_va[2][2] = 1;  // Bias
	f_va[0][3] = u[0];                         // Scale factor
	f_va[1][4] = u[1];
	f_va[2][5] = u[2];
	f_va[0][6] = -u[1];                        // Misalignment
	f_va[0][7] = u[2];
	f_va[1][8] = u[0];
	f_va[1][9] = -u[2];
	f_va[2][10] = -u[0];
	f_va[2][11] = u[1];
	//print(f_va);

	f_va = mult(c_b2e, f_va);
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 12; ++j) {
			f[3+i][10+j] += dt*f_va[i][j];
		}
	}

	// Attitude eq - attitude term
	vector3d g {{ u[3], u[4], u[5] }};
	scalar g0 = 2*(-g[0] - g[1] - g[2]);
	scalar g1 = 2*(-g[0] - g[1] + g[2]);
	scalar g2 = 2*(-g[0] + g[1] - g[2]);
	scalar g3 = 2*(-g[0] + g[1] + g[2]);
	scalar g4 = 2*( g[0] - g[1] - g[2]);
	scalar g5 = 2*( g[0] - g[1] + g[2]);
	scalar g6 = 2*( g[0] + g[1] - g[2]);

	matrix3x4d g_mat {{ {{ g1, g0, g5, g4}},
		                {{ g4, g2, g0, g6}},
		                {{ g2, g3, g1, g0}} }};

	matrix4x3d b {{ {{-q_b2e[1], -q_b2e[2], -q_b2e[3]}},
		            {{ q_b2e[0], -q_b2e[3],  q_b2e[2]}},
		            {{ q_b2e[3],  q_b2e[0], -q_b2e[1]}},
		            {{-q_b2e[2],  q_b2e[1],  q_b2e[0]}} }};
	matrix4d c = mult(b, g_mat);

	f[6][6] += 0.5*dt*c[0][0];
	f[6][7] += 0.5*dt*c[0][1];
	f[6][8] += 0.5*dt*c[0][2];
	f[6][9] += 0.5*dt*c[0][3];
	f[7][6] += 0.5*dt*c[1][0];
	f[7][7] += 0.5*dt*c[1][1];
	f[7][8] += 0.5*dt*c[1][2];
	f[7][9] += 0.5*dt*c[1][3];
	f[8][6] += 0.5*dt*c[2][0];
	f[8][7] += 0.5*dt*c[2][1];
	f[8][8] += 0.5*dt*c[2][2];
	f[8][9] += 0.5*dt*c[2][3];
	f[9][6] += 0.5*dt*c[3][0];
	f[9][7] += 0.5*dt*c[3][1];
	f[9][8] += 0.5*dt*c[3][2];
	f[9][9] += 0.5*dt*c[3][3];

	matrix<3,12> f_qg = get_zero<3,12>();
	f_qg[0][0] = f_qg[1][1] = f_qg[2][2] = 1;  // Bias
	f_qg[0][3] = u[3];                         // Scale factor
	f_qg[1][4] = u[4];
	f_qg[2][5] = u[5];
	f_qg[0][6] = -u[4];                        // Misalignment
	f_qg[0][7] = u[5];
	f_qg[1][8] = u[3];
	f_qg[1][9] = -u[5];
	f_qg[2][10] = -u[3];
	f_qg[2][11] = u[4];
	matrix<4,12> act_f_qg = mult(b, f_qg);
	printf("Act F_qg\n");
	print(act_f_qg);

	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 12; ++j) {
			f[6+i][22+j] -= dt*act_f_qg[i][j];
		}
	}

	return f;
}

a_matrix_ms a_get_h_ekf()
{
	a_matrix_ms h = get_zero<a_m_siz, a_s_siz>();

	h[0][0] = h[1][1] = h[2][2] = h[3][3] = h[4][4] = h[5][5] = 1.0;

	return h;
}

void a_ekf_iteration(Nav_State ns, vector6d u, a_vector_m y, scalar dt, bool new_meas)
{
	printf("Cov\n");
	print(a_ekf_cov);

	// Prediction
	a_ekf_state = a_state_transition(a_ekf_state, ns.pos, ns.att, u, dt);
	printf("x_pred\n");
	print(a_ekf_state);

	a_matrix_ss f = a_get_f_ekf(ns.pos, ns.att, u, dt);
	a_matrix_ms h = a_get_h_ekf();
	printf("f_k\n");
	print(f);
	printf("h_k\n");
	print(h);

	//a_matrix_ss q_now = get_q_k(ekf_q_k, ns.att, dt);
	a_matrix_ss q_now = a_ekf_init_q_disc();

	a_ekf_cov = mult(f, a_ekf_cov);
	a_ekf_cov = mult(a_ekf_cov, transpose(f));
	a_ekf_cov = add(a_ekf_cov, q_now);
	printf("cov_pred\n");
	print(a_ekf_cov);

	if ( !new_meas ) {
		return;
	}

	// Filtering
	a_matrix_mm g1 = mult(mult(h, a_ekf_cov), transpose(h));
	g1 = add(g1, a_ekf_r_k);
	g1 = inv_sim_pd(g1);
	printf("(HPH^T+R)^-1\n");
	print(g1);
	a_matrix_sm gain = mult(mult(a_ekf_cov, transpose(h)), g1);
	printf("gain\n");
	print(gain);

	a_vector_m d_meas = mult(h, a_ekf_state);
	d_meas = sub(y, d_meas);
	a_vector_s d_state = mult(gain, d_meas);
	a_ekf_state = add(a_ekf_state, d_state);
	printf("x_filt\n");
	print(a_ekf_state);

	a_matrix_ss c1 = sub(identity<a_s_siz>(), mult(gain, h));
	a_matrix_ss c2 = mult(c1, a_ekf_cov);
	c1 = mult(c2, transpose(c1));
	c2 = mult(mult(gain, a_ekf_r_k), transpose(gain));
	a_ekf_cov = add(c1, c2);

	// Guarantee cov is symmetric and positive definite
	a_ekf_cov = force_symmetry(a_ekf_cov);

	printf("Cov\n");
	print(a_ekf_cov);

	if ( check_nan(a_ekf_cov) )
		std::cin.get();
}

Nav_State a_ekf(Nav_State curr, vector9d imu_meas, vector6d gnss_meas, scalar dt,
		bool new_meas, FILE* err_f)
{
	_LOG("AEKF");
	//scalar dt = 1;

	vector6d u {{ imu_meas[0], imu_meas[1], imu_meas[2], 
		          imu_meas[3], imu_meas[4], imu_meas[5] }};

	a_vector_m y = a_get_kf_meas(gnss_meas, curr);
	_LOG_6D("GPS Meas residue", y);

	a_ekf_iteration(curr, u, y, dt, new_meas);
	printf("EKF_State\n");
	print(a_ekf_state);

	//check_constraints_v2(dt);

	curr = a_correct_nav_state(curr, a_ekf_state);
	if ( new_meas )
		a_save_err_state(a_ekf_state, err_f);

	// Reset state, so estimated error is not accounted for twice
	a_ekf_state = a_reset_state();

	return curr;
}
