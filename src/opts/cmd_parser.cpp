#include <opts/cmd_parser.h>

std::unordered_map<const char*, const char*> get_cmd_opts(
        const int argc, const char* argv[], const expected_opt* opts)
{
	std::unordered_map<const char*, const char*> opt_args;

	get_cmd_opts(argc, argv, opts, opt_args);

	return opt_args;
}

void get_cmd_opts(const int argc, const char* argv[], const expected_opt* opts,
                  std::unordered_map<const char*, const char*>& opt_args)
{
	for (size_t i = 0; opts[i].long_opt; i++) {
		compare_cmd_args_against_current_opt(opts[i], argc, argv,
			opt_args);
	}

}

inline void compare_cmd_args_against_current_opt(
        const expected_opt& curr_opt, const int argc, const char* argv[],
        std::unordered_map<const char*, const char*>& opt_args)
{
	for (int j = 1; j < argc; j++) {
		if (!arg_is_flag(argv[j])) continue;

		// Decide if long opt or short opt
		if (flag_is_long_opt(argv[j])) {
			// Long opt, actual opt name starts after "--"
			if (strcmp(curr_opt.long_opt, &argv[j][2]) == 0) {
				save_opt(curr_opt, argc, argv, j, opt_args);
			}
		} else {
			// Short opt, actual opt name starts after '-'
			if (curr_opt.short_opt == argv[j][1]) {
				save_opt(curr_opt, argc, argv, j, opt_args);
			}
		}
	}
}

int arg_is_flag(const char* this_arg) { return this_arg[0] == '-'; }

inline int flag_is_long_opt(const char* this_arg) { return this_arg[1] == '-'; }

void save_opt(const expected_opt& opt, const int argc, const char* argv[],
              int&                                          index,
              std::unordered_map<const char*, const char*>& opt_args)
{
	if (opt.needs_arg)
		save_arg_if_it_exists(opt, argc, argv, index, opt_args);
	else
		save_empty_arg(opt, opt_args);
}

inline void save_arg_if_it_exists(
        const expected_opt& opt, const int argc, const char* argv[],
        int& index, std::unordered_map<const char*, const char*>& opt_args)
{
	// Check only following command argument
	if (argc > index + 1) {
		if (!arg_is_flag(argv[index + 1])) {
			// If next command argument is not a flag,
			// it's an argument to the current flag.
			opt_args[opt.long_opt] = argv[index + 1];
			index++;
			return;
		}
	}

	printf("%s requires an argument. Ignoring flag...", argv[index]);
}

inline void save_empty_arg(const expected_opt&                           opt,
                    std::unordered_map<const char*, const char*>& opt_args)
{
	opt_args[opt.long_opt] = "\0";
}
