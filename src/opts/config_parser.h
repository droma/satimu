#ifndef _CONFIG_PARSER_H_
#define _CONFIG_PARSER_H_

#include <string>
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/filesystem.hpp>

#include <utils/log.h>

typedef boost::property_tree::ptree  ptree_t;
typedef boost::filesystem::path      path_t;

class Config_Parser
{
	private:
		ptree_t prop_tree;

		ptree_t read_configuration(std::string config_path);
		template <class T>
			T try_to_get_prop(std::string prop);

	public:
		Config_Parser(std::string config_path);
		template <class T>
			void get_prop(T* out, std::string prop);
		bool check_if_prop_exists(std::string prop);

};

template <class T>
T Config_Parser::try_to_get_prop(std::string prop)
{
	boost::optional<ptree_t&> child = prop_tree.get_child_optional(prop);
	if( !child ) {
		throw prop;
	} else {
		return prop_tree.get <T> (prop);
	}
}

template <class T>
void Config_Parser::get_prop(T* out, std::string prop)
{
	try {
		*out =  try_to_get_prop <T> (prop);
	} catch (std::string prop_not_found) {
		_LOG_V("Couldn't find config entry", prop_not_found)
	}
}

#endif
