#include <opts/config_parser.h>

Config_Parser::Config_Parser(std::string config_path)
{
	//_LOG(config_path)
	prop_tree = read_configuration(config_path);
}

ptree_t Config_Parser::read_configuration(std::string config_path)
{
	ptree_t prop_tree;

	boost::property_tree::ini_parser::read_ini(config_path, prop_tree);

	return prop_tree;
}

bool Config_Parser::check_if_prop_exists(std::string prop)
{
	boost::optional<ptree_t&> child = prop_tree.get_child_optional(prop);

	if (child)
		return true;
	else
		return false;
}

