#ifndef __CMD_OPTS_PARSER_H__
#define __CMD_OPTS_PARSER_H__

#include <stdio.h>
#include <string.h>

#include <unordered_map>
#include "utils/log.h"

//#include <hash_table.h>

#define NO_ARGUMENT_REQUIRED 0
#define ARGUMENT_REQUIRED    1

struct __expected_opt {
	const char *long_opt;
	char       short_opt;
	int        needs_arg;
};
typedef struct __expected_opt expected_opt;

std::unordered_map <const char*, const char*> get_cmd_opts(const int argc,
		const char* argv[], const expected_opt* opts);

void get_cmd_opts(const int argc, const char* argv[],
		const expected_opt* opts,
		std::unordered_map <const char*, const char*>& opt_args);

inline void compare_cmd_args_against_current_opt(const expected_opt& curr_opt,
		const int argc, const char* argv[],
		std::unordered_map <const char*, const char*>& opt_args);

int arg_is_flag(const char* argv);

inline int flag_is_long_opt(const char* argv);

void save_opt(const expected_opt& opt, const int argc, const char* argv[],
		int& index, std::unordered_map <const char*, const char*>& opt_args);

inline void save_arg_if_it_exists (const expected_opt& opt, const int argc, 
		const char* argv[], int& index, 
		std::unordered_map <const char*, const char*>& opt_args);

inline void save_empty_arg(const expected_opt& opt,
		std::unordered_map <const char*, const char*>& opt_args);

#endif
