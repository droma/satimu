#include <alignment.h>

//void fine_alignment(FILE* imu_file, FILE* gps_file, FILE* real_file, 
		//Nav_State& nav_state, Ports sensors, bool file_mode, 
		//std::string filter, int freq)
//{
	//int alignment_done = 0;
	
	//init_ukf();
	//init_ekf();
	
	//int i = 0;
	//vector9d imu_meas;
	//vector6d gnss_meas;

	//initialize(imu_file, gps_file, real_file, nav_state, sensors, file_mode, freq);
	//i++;

	//vector4d prev_att {{1, 0, 0, 0}};
	//while ( alignment_done < 10 ) {
		//// get_meas
		//if (file_mode) {
			//imu_meas  = get_imu_from_file(imu_file);
			//if (i%freq == 0) gnss_meas = get_gps_from_file(gps_file);

			//vector3d dummy;
			//fscanf(real_file, "%lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2]);
		//} else {
			////printf("Getting IMU msg\n");
			//imu_meas = parse_imu(sensors.imu);
			////printf("Getting GPS msg\n");
			//if (i%freq == 0) gnss_meas = parse_gnss(sensors.gnss);
		//}

		//// kf
		//if (filter.compare("ukf") == 0 && i%freq == 0)
			//nav_state = ukf(nav_state, imu_meas, gnss_meas);
		//if (filter.compare("ekf") == 0 && i%freq == 0)
			//nav_state = ekf(nav_state, imu_meas, gnss_meas);

		//// check convergency
		//if (i%freq == 0) {
			//vector4d err_att = mult(nav_state.att, conjugate(prev_att));
			//scalar e = 1e-2;
			//if ( acos(err_att[0]) < e && err_att[1] < e && err_att[2] < e && err_att[3] < e)
				//alignment_done++;
			//else
				//alignment_done = 0;
			//prev_att = nav_state.att;

			//printf("Err att @ it = %d\n", i);
			//print(err_att);
			////std::cin.get();
		//}

		//i++;
	//}
	//nav_state.pos = {{ gnss_meas[0], gnss_meas[1], gnss_meas[2]}};
	//nav_state.vel = {{ gnss_meas[3], gnss_meas[4], gnss_meas[5]}};
	//printf("Attitude\n");
	//print(nav_state.att);
	////std::cin.get();
//}

void initialize(FILE* imu_file, FILE* gps_file, FILE* real_file, 
		Nav_State& nav_state, Ports sensors, bool file_mode, int freq)
{
	vector9d imu_meas;
	vector6d gnss_meas;

	//printf("Initialization\n");
	vector3d mean_acc  {{0, 0, 0}};
	vector3d mean_gyro {{0, 0, 0}};
	const int init_count = 200;
	for (int i = 0; i < init_count; ++i) {
		if (file_mode) {
			imu_meas  = get_imu_from_file(imu_file);
			if (i%freq == 0) gnss_meas = get_gps_from_file(gps_file);

			if ( real_file != NULL ) {
				vector3d dummy;
				fscanf(real_file, "%lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2]);
			}
		} else {
			//printf("Getting IMU msg\n");
			imu_meas = parse_imu(sensors.imu);
			//printf("Getting GPS msg\n");
			if (i%freq == 0) gnss_meas = parse_gnss(sensors.gnss);
		}
		printf("ACC: %f, %f, %f\n", imu_meas[0], imu_meas[1], imu_meas[2]);
		printf("GYR: %f, %f, %f\n", imu_meas[3], imu_meas[4], imu_meas[5]);

		//std::cout << "Real pos:" << line << '\n';

		mean_acc[0]  += imu_meas[0];
		mean_acc[1]  += imu_meas[1];
		mean_acc[2]  += imu_meas[2];
		mean_gyro[0] += imu_meas[3];
		mean_gyro[1] += imu_meas[4];
		mean_gyro[2] += imu_meas[5];

	}
	nav_state.pos = {{ gnss_meas[0], gnss_meas[1], gnss_meas[2]}};

	vector3d llh0 = ecef2llh(nav_state.pos);
	//nav_state.pos = llh2ecef(llh0);
	//_LOG_3D("LLH0", llh0);
	//_LOG_3D("POS0", nav_state.pos);

	for (int i = 0; i < 3; ++i) {
		mean_acc[i]  = mean_acc[i]/init_count;
		mean_gyro[i] = mean_gyro[i]/init_count;
	}

	// Farrell2008 p417
	//_LOG_3D("Acc",  mean_acc);
	//_LOG_3D("Gyro", mean_gyro);
	const vector3d cross_ag = cross(mean_acc, mean_gyro);
	//_LOG_3D("Cross acc gyro", cross_ag);

	vector3d g_vec = grav_model_ecef(llh0);
	scalar   grav  = get_norm(g_vec);

    const scalar t02 = 1/grav;
    const scalar t00 = tan(llh0[0])*t02;
    const scalar t10 = 1/(datum_e_rot*cos(llh0[0]));
    const scalar t21 = t10*t02;

    //_LOG_V("T00", t00);
    //_LOG_V("T02", t02);
    //_LOG_V("T10", t10);
    //_LOG_V("T21", t21);

	matrix3d r_n2b
	{{ {{mean_acc[0]*t00 + mean_gyro[0]*t10, cross_ag[0]*t21, mean_acc[0]*t02}},
	   {{mean_acc[1]*t00 + mean_gyro[1]*t10, cross_ag[1]*t21, mean_acc[1]*t02}},
	   {{mean_acc[2]*t00 + mean_gyro[2]*t10, cross_ag[2]*t21, mean_acc[2]*t02}} }};
    //_LOG_3D("r_n2b[0]", r_n2b[0]);
    //_LOG_3D("r_n2b[0]", r_n2b[1]);
    //_LOG_3D("r_n2b[0]", r_n2b[2]);

	vector4d q_n2b;
	q_n2b = dircos_to_quat(r_n2b);
	q_n2b = conv_to_unit(q_n2b);
	//_LOG_4D("Q_n2b", q_n2b);

	// Because q_n2b is unit quaternion, inverse = conjugate
	vector4d q_b2n = conjugate(q_n2b);

	// Kuipers2002 p167
	scalar ax = ( llh0[0] - M_PI/2)/2;
	scalar az = (-llh0[1] - M_PI/2)/2;

	scalar cx = cos(ax);
	scalar sx = sin(ax);
	scalar cz = cos(az);
	scalar sz = sin(az);

	vector4d q_n2e;
	scalar k = sqrt(2)/2;
	q_n2e[0] =  k*sx*(sz-cz);
	q_n2e[1] =  k*cx*(cz+sz);
	q_n2e[2] =  k*cx*(cz-sz);
	q_n2e[3] = -k*sx*(cz+sz);
	//_LOG_4D("Q_n2e", q_n2e);
	vector4d q_b2e = mult(q_b2n, q_n2e);

	_LOG_4D("q_b2e", q_b2e);

	// Because previous q_b2e is rotation of the fram, not of the vector
	q_b2e[1] = -q_b2e[1];
	q_b2e[2] = -q_b2e[2];
	q_b2e[3] = -q_b2e[3];

	nav_state.att = q_b2e;

	// Await input
	//std::cin.get();
}

