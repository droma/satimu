#include <ukf.h>

vector_s ukf_state;
matrix_ss cov;
matrix_ww q_k;
matrix_mm r_k;

void init_ukf()
{
	ukf_state = reset_state();

	cov = init_cov();
	//q_k = init_q();
	r_k = init_r();
}

vector_m predict_meas_from_state(vector_s s)
{
	vector_m z;

	z[0] = s[0];
	z[1] = s[1];
	z[2] = s[2];
	z[3] = s[3];
	z[4] = s[4];
	z[5] = s[5];

	return z;
}

matrix_sig_s get_sigma_points(matrix_ss cov, int n, scalar fact)
{
	matrix_sig_s sigma;

	sigma[0] = ukf_state;

	matrix_ss sqrt_cov = cholesky(cov);
	printf("SQRT(Cov)\n");
	print(sqrt_cov);

	scalar k = sqrt(fact);
	printf("K: %lf\n", k);
	//printf("UKF_State in get sigma points\n");
	//print(ukf_state);
	for (int i = 1; i <= n; ++i)
		for (int j = 0; j < n; ++j)
			sigma[i][j] = ukf_state[j] + k*sqrt_cov[i-1][j];

	for (int i = n+1; i < 2*n+1; ++i)
		for (int j = 0; j < n; ++j)
			sigma[i][j] = ukf_state[j] - k*sqrt_cov[i-n-1][j];

	return sigma;
}

struct weights {
	scalar m0, m, c0, c;
};

struct weights calculate_weights(scalar kappa, const int n)
{
	struct weights w;

	w.m0 = kappa/(n + kappa);
	w.m  = 0.5/(n + kappa);
	w.c0 = kappa/(n + kappa);
	w.c  = 0.5/(n + kappa);

	return w;
}

void ukf_iteration(Nav_State ns, vector6d u, vector_m y, scalar dt, bool new_meas)
{
	const int n   = s_siz;     // Size of state vector
	const int m   = m_siz;     // Size of measurments
	const int s_n = sig_siz;   // Number of sigma points
	matrix_sig_s sigma;        // Sigma points

	// Parameters
	//scalar kappa     = 0.7;
	//scalar kappa     = 0.2;
	scalar kappa     = 0;
	struct weights w = calculate_weights(kappa, n);

	sigma = get_sigma_points(cov, n, n+kappa);
	printf("Sigma\n");
	print(sigma);

	// Prediction
	matrix_sig_s sigma_pred;
	for (int i = 0; i < s_n; ++i)
		sigma_pred[i] = state_transition(sigma[i], ns.pos, ns.att, u, dt);

	printf("Sigma_pred = \n");
	print(sigma_pred);

	// Predict state
	vector_s x_pred;
	for (int i = 0; i < n; ++i) {
		x_pred[i] = w.m0*sigma_pred[0][i];
		for (int j = 1; j < s_n; ++j)
			x_pred[i] += w.m*sigma_pred[j][i];
	}

	printf("X_pred = \n");
	print(x_pred);

	if ( !new_meas ) {
		ukf_state = x_pred;
		return;
	}

	//matrix_ss q_now = get_q_k(q_k, ns.att, dt);
	matrix_ss q_now = init_q_disc();

	// Predict state covariance
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < n; ++j) {
			cov[i][j] = q_now[i][j] + w.c0*(sigma_pred[0][i]
					- x_pred[i])*(sigma_pred[0][j] - x_pred[j]);
			for (int k = 1; k < s_n; ++k)
				cov[i][j] += w.c*(sigma_pred[k][i]
						- x_pred[i])*(sigma_pred[k][j] - x_pred[j]);
		}
	}

	// Get new sigma points
	sigma = get_sigma_points(cov, n, n+kappa);

	printf("S_y = \n");
	print(sigma);

	// Measurement prediction
	matrix_sig_m sigma_y;
	for (int i = 0; i < s_n; ++i)
		sigma_y[i] = predict_meas_from_state(sigma[i]);

	printf("S_pred = \n");
	print(sigma_y);

	vector_m y_pred;
	for (int i = 0; i < m; ++i) {
		y_pred[i] = w.m0*sigma_y[0][i];
		for (int j = 1; j < s_n; ++j)
			y_pred[i] += w.m*sigma_y[j][i];
	}

	//printf("Y_p = \n");
	//print(y_pred);
	//printf("Y = \n");
	//print(y);

	//matrix_mm cov_yy;
	//for (int i = 0; i < m; ++i) {
		//for (int j = 0; j < m; ++j) {
			//cov_yy[i][j] = r_k[i][j] + w.c0*(sigma_y[0][i]
					//- y_pred[i])*(sigma_y[0][j] - y_pred[j]);
			//for (int k = 1; k < s_n; ++k)
				//cov_yy[i][j] += w.c*(sigma_y[k][i]
						//- y_pred[i])*(sigma_y[k][j] - y_pred[j]);
		//}
	//}

	//matrix_mm cov_yy_I = inv_sim_pd(cov_yy);

	//matrix_sm cov_xy;
	//for (int i = 0; i < n; ++i) {
		//for (int j = 0; j < m; ++j) {
			//cov_xy[i][j] = w.c0*(sigma_pred[0][i] -
					//x_pred[i])*(sigma_y[0][j] - y_pred[j]);
			//for (int k = 1; k < s_n; ++k)
				//cov_xy[i][j] += w.c*(sigma_pred[k][i] -
						//x_pred[i])*(sigma_y[k][j] - y_pred[j]);
		//}
	//}
	//printf("Cov_yy\n");
	//print(cov_yy);
	//printf("Cov_yy^-1\n");
	//print(cov_yy_I);
	//printf("Cov_xy\n");
	//print(cov_xy);

	//// Kalman gain
	//matrix_sm gain = mult(cov_xy, cov_yy_I);
	//printf("K_gain\n");
	//print(gain);

	// Alternative for linear obs
	matrix_ms h = get_zero<m_siz, s_siz>();
	h[0][0] = h[1][1] = h[2][2] = h[3][3] = h[4][4] = h[5][5] = 1.0;

	matrix_mm s = mult(h, mult(cov,transpose(h)));
	s = add(s,r_k);
	s = inv_sim_pd(s);
	matrix_sm gain = mult(cov, transpose(h));
	gain = mult(gain, s);

	// State update
	vector_m y_res = sub(y, y_pred);
	for (int i = 0; i < n; ++i) {
		ukf_state[i] = x_pred[i];
		for (int j = 0; j < m; ++j)
			ukf_state[i] += gain[i][j]*y_res[j];
	}

	//printf("Y\n");
	//print(y);
	//printf("Y_pred\n");
	//print(y_pred);
	printf("Y_res\n");
	print(y_res);

	// Covariance update
	//matrix_sm dcov1 = mult(gain, cov_yy);
	//matrix_ms gain_t = transpose(gain);
	//matrix_ss dcov  = mult(dcov1, gain_t);
	//cov = sub(cov, dcov);
	//printf("K_gain*Cov_yy\n");
	//print(dcov1);
	//printf("K_gain^T\n");
	//print(gain_t);
	//printf("K_gain*Cov_yy*K_gain^T\n");
	//print(dcov);

	// Joseph form
	matrix_ss cov_fact = identity<s_siz>();
	cov_fact = sub(cov_fact, mult(gain, h));
	cov = mult(mult(cov_fact, cov), transpose(cov_fact));
	matrix_ss cov_r_term = mult(mult(gain, r_k), transpose(gain));
	cov = add(cov, cov_r_term);
	
	//matrix_ss id = identity<s_siz>();
	//matrix_ss dcov = sub(id, mult(gain, h));
	//cov = mult(dcov, cov);

	// Guarantee cov is symmetric and positive definite
	cov = force_symmetry(cov);

	printf("Cov\n");
	print(cov);

	// Normalize quaternion components
	vector4d q_norm = {{ ukf_state[6], ukf_state[7], ukf_state[8], ukf_state[9]}};
	q_norm = conv_to_unit(q_norm);
	ukf_state[6] = q_norm[0];
	ukf_state[7] = q_norm[1];
	ukf_state[8] = q_norm[2];
	ukf_state[9] = q_norm[3];

	// Check NAN in cov
	if ( check_nan(cov) )
		std::cin.get();
}

Nav_State ukf(Nav_State curr, vector9d imu_meas, vector6d gnss_meas, scalar dt,
		bool new_meas, FILE* err_f)
{
	_LOG("UKF");
	//scalar dt = 0.01;

	vector6d u {{ imu_meas[0], imu_meas[1], imu_meas[2], 
		          imu_meas[3], imu_meas[4], imu_meas[5] }};

	vector_m y = get_kf_meas(gnss_meas, curr);
	printf("GPS Meas residue\n");
	print(y);

	ukf_iteration(curr, u, y, dt, new_meas);
	printf("UKF_State\n");
	print(ukf_state);

	//check_constraints_v2(dt);

	curr = correct_nav_state(curr, ukf_state);
	if ( new_meas )
		save_err_state(ukf_state, err_f);

	// Reset state, so estimated error is not accounted for twice
	ukf_state = reset_state();

	return curr;
}
