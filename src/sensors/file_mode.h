#ifndef __MEAS_FROM_FILE_H__
#define __MEAS_FROM_FILE_H__

#include <fstream>
#include <iostream>
#include <string>
#include <cstdint>
#include <cstdio>

#include <sensors/sensors.h>
#include <utils/algebra.h>
#include <utils/log.h>
#include <nav_state.h>

vector6d get_gps_from_file(FILE* gps_file);
vector9d get_imu_from_file(FILE* imu_file);
void close_files(FILE** imu_f, FILE** gps_f, FILE** real_f, FILE** out_f, 
		FILE** err_f);
bool setup_files (std::string folder, FILE** imu_f, FILE** gps_f, FILE** real_f,
		FILE** out_f, FILE** err_f, std::string alg);
void compare_estimate_to_reality(Nav_State nav_state, FILE* real_f, 
		FILE* out_f);
void print_rmse();

#endif
