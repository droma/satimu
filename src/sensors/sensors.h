#ifndef __SENSORS_H__
#define __SENSORS_H__

#include <errno.h>   // Error number definitions
#include <fcntl.h>   // File control definitions
#include <string.h>  // String function definitions
#include <array>
#include <cstring>

#if _WIN32
	#include <io.h>
#else
	#include <termio.h>  // POSIX terminal control definitions
	#include <unistd.h>
#endif

#include <stdlib.h>

#include <options.h>
#include <utils/log.h>
#include <utils/algebra.h>

struct Ports {
	int imu  = -1;
	int gnss = -1;
};

//void set_port_options(int port)
//int open_port(char* port_path);
Ports sensor_setup(Sensor_Options& opts);
char* readl(int p);
void flush_sensor_port(int port);
vector9d parse_imu(int port);
vector6d parse_gnss(int port);

scalar lat_str_to_num(char lat[16], char sign_lat);
scalar lon_str_to_num(char lon[16], char sign_lon);

void read_gpgga_from_cstr(char* line, char lat[16], char& sign_lat, 
		char lon[16], char& sign_lon, char h[16]);

#endif
