#include <sensors/file_mode.h>

vector9d get_imu_from_file(FILE *imu_file)
{
	vector9d meas;
	//std::string line;
	scalar t;

	//std::getline(imu_file, line);
	//printf(">> %s\n", line.c_str());
	////std::sscanf(line.c_str(), "$INS,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\r", &meas[0],
	//std::sscanf(line.c_str(), "$INS,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf\r", &meas[0],
			//&meas[1], &meas[2], &meas[3], &meas[4], &meas[5], &meas[6],
			//&meas[7], &meas[8], &t);
	fscanf(imu_file, "$INS,%lf,%lf,%lf,%lf,%lf,%lf,%lf\r", &meas[0],
			&meas[1], &meas[2], &meas[3], &meas[4], &meas[5], &t);
	//fscanf(imu_file, "$INS,%lf,%lf,%lf,%lf,%lf,%lf,%lf\r", &meas[0],
			//&meas[1], &meas[2], &meas[3], &meas[4], &meas[5], &meas[6],
			//&meas[7], &meas[8], &t);

	return meas;
}

void fread(FILE *f, void *buf, size_t nbytes)
{
	uint8_t c[4];

	for (unsigned int i = 0; i < nbytes; ++i) {
		c[i] = fgetc(f);
		//printf("%d\n", c[i]);
	}
	memcpy(buf, c, nbytes);
}

vector6d read_ubx_nav_sol(FILE *f)
{
	vector6d meas;

	if (feof(f)) {
		_LOG("EOF");
		std::cin.get();
	}

	bool done = false;
	while ( !done ) {

		if ( feof(f) ) {
			_LOG("EOF");
			return {{0, 0, 0, 0, 0, 0}};
		}

		// Retrieve headers 0xB5 0x62
		uint8_t h1;
		fread(f, &h1, 1);
		uint8_t h2;
		fread(f, &h2, 1);

		// If header is not found, skip and try again
		if ( h1 != 0xB5 || h2 != 0x62)
			continue;

		//_LOG("UBX MSG");

		// Search for Nav-Sol message
		uint8_t id1;
		fread(f, &id1, 1);
		uint8_t id2;
		fread(f, &id2, 1);
		if ( id1 != 0x01 || id2 != 0x06 )
			continue;

		//_LOG("NAV-SOL MSG");

		unsigned short len;
		fread(f, &len, sizeof(len));
		//_LOG_V("LEN", len);

		uint32_t itow;
		fread(f, &itow, sizeof(itow));
		//_LOG_V("ITOW", itow);

		int32_t ftow;
		fread(f, &ftow, sizeof(ftow));
		//_LOG_V("FTOW", ftow);

		int16_t week;
		fread(f, &week, sizeof(week));
		//_LOG_V("WEEK", week);

		uint8_t gpsfix;
		fread(f, &gpsfix, sizeof(gpsfix));
		//_LOG_V("GPSFIX", gpsfix);

		uint8_t flag;
		fread(f, &flag, sizeof(flag));
		//_LOG_V("FLAG", flag);

		int32_t ecefx;
		fread(f, &ecefx, sizeof(ecefx));
		//_LOG_V("ECEFx", ecefx);

		int32_t ecefy;
		fread(f, &ecefy, sizeof(ecefy));
		//_LOG_V("ECEFy", ecefy);

		int32_t ecefz;
		fread(f, &ecefz, sizeof(ecefz));
		//_LOG_V("ECEFz", ecefz);

		uint32_t pacc;
		fread(f, &pacc, sizeof(pacc));
		//_LOG_V("PACC", pacc);

		int32_t ecefvx;
		fread(f, &ecefvx, sizeof(ecefvx));
		//_LOG_V("ECEFVx", ecefvx);

		int32_t ecefvy;
		fread(f, &ecefvy, sizeof(ecefvy));
		//_LOG_V("ECEFVy", ecefvy);

		int32_t ecefvz;
		fread(f, &ecefvz, sizeof(ecefvz));
		//_LOG_V("ECEFVz", ecefvz);

		uint32_t sacc;
		fread(f, &sacc, sizeof(sacc));
		//_LOG_V("SACC", sacc);

		uint16_t pdop;
		fread(f, &pdop, sizeof(pdop));
		//_LOG_V("PDOP", pdop);

		uint8_t res1;
		fread(f, &res1, sizeof(res1));
		//_LOG_V("RES1", res1);

		uint8_t numsv;
		fread(f, &numsv, sizeof(numsv));
		//_LOG_V("NUMSV", numsv);

		uint32_t res2;
		fread(f, &res2, sizeof(res2));
		//_LOG_V("RES2", res2);

		uint8_t cs_a;
		fread(f, &cs_a, sizeof(cs_a));
		//_LOG_V("CSA", cs_a);

		uint8_t cs_b;
		fread(f, &cs_b, sizeof(cs_b));
		//_LOG_V("CSB", cs_b);
		
		if (gpsfix == 0)
			return {{0, 0, 0, 0, 0, 0}};

		// Converts meas from cm to m
		meas[0] = 0.01*((scalar) ecefx);
		meas[1] = 0.01*((scalar) ecefy);
		meas[2] = 0.01*((scalar) ecefz);
		meas[3] = 0.01*((scalar) ecefvx);
		meas[4] = 0.01*((scalar) ecefvy);
		meas[5] = 0.01*((scalar) ecefvz);

		done = true;
	}

	return meas;
}

vector6d get_gps_from_file(FILE *gps_file)
{
	vector6d meas;

	//char lat[32], lon[32], sign_lat, sign_lon, h[32];

	//std::string line;
	//std::getline(gps_file, line);
	//char cline[256];
	//strcpy(cline, line.c_str());

	//_LOG("Before Read gpgga from cstr");
	//read_gpgga_from_cstr(cline, lat, sign_lat, lon, sign_lon, h);

	//_LOG("Lat/Lon str to num");
	//meas[1] = lon_str_to_num(lon, sign_lon);
	//meas[0] = lat_str_to_num(lat, sign_lat);
	//meas[2] = atof(h);
	//_LOG_3D("LLH", meas);

	meas = read_ubx_nav_sol(gps_file);

	return meas;
}

void close_files(FILE** imu_f, FILE** gps_f, FILE** real_f, FILE** out_f, 
		FILE** err_f)
{
	fclose(*imu_f);
	fclose(*gps_f);
	if ( *real_f != NULL)
		fclose(*real_f);
	fclose(*out_f);
	fclose(*err_f);
}

bool setup_files (std::string folder, FILE** imu_f, FILE** gps_f, FILE** real_f,
		FILE** out_f, FILE** err_f, std::string alg)
{
	std::string real_file_path = folder + "/real_ecef.meas";
	std::string imu_file_path  = folder + "/imu.meas";
	std::string gps_file_path  = folder + "/gps.meas";
	
	std::string out_file_path;
	if (alg[0] == ' ')
		out_file_path  = folder + "/out.meas";
	else
		out_file_path  = folder + "/out_" + alg + ".meas";

	std::string err_file_path;
	if (alg[0] == ' ')
		err_file_path  = folder + "/err.meas";
	else
		err_file_path  = folder + "/err_" + alg + ".meas";

	*real_f = fopen(real_file_path.c_str(), "r");
	if(*real_f == NULL) {
		printf("Error in opening real file. Continuing...\n");
		//return 1;
	}
	*imu_f  = fopen(imu_file_path.c_str(),  "r");
	if(*imu_f == NULL) {
		perror("Error in opening imu file. Aborting...");
		return 1;
	}
	*gps_f  = fopen(gps_file_path.c_str(),  "r");
	if(*gps_f == NULL) {
		perror("Error in opening gps file. Aborting...");
		return 1;
	}
	*out_f  = fopen(out_file_path.c_str(),  "w");
	if(*out_f == NULL) {
		perror("Error in opening output file. Aborting...");
		return 1;
	}
	*err_f  = fopen(err_file_path.c_str(),  "w");
	if(*err_f == NULL) {
		perror("Error in opening error state file. Aborting...");
		return 1;
	}

	//printf("Files are fine enough to continue.\n");
	return 0;
}

double accumulator_err = 0;
int    num_points = 0;

void compare_estimate_to_reality(Nav_State nav_state, FILE* real_f, FILE* out_f)
{
	vector3d real_pos;

	//printf("Did I get here?\n");
	if ( real_f != NULL ) {
		fscanf(real_f, "%lf %lf %lf\n", &real_pos[0], &real_pos[1], 
				&real_pos[2]);
		_LOG_3D("Real pos", real_pos);

		for (int i = 0; i < 3; ++i) {
			real_pos[i] -= nav_state.pos[i];
		}
		_LOG_3D("Real - Est", real_pos);
		const double pos_err = get_norm(real_pos);
		_LOG_V("|Real - Est|", pos_err);

		// For RMSE
		num_points++;
		accumulator_err += pos_err*pos_err;
	}

	fprintf(out_f, "%lf %lf %lf %lf %lf %f\n", nav_state.pos[0], nav_state.pos[1],
			nav_state.pos[2], nav_state.vel[0], nav_state.vel[1], nav_state.vel[2]);
}

void print_rmse()
{
	double rmse = sqrt(accumulator_err/num_points);
	
	//_LOG_V("acc", accumulator_err);
	//_LOG_V("num points", num_points);
	_LOG_V("POS RMSE", rmse);
}
