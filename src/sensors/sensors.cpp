#include <sensors/sensors.h>

#ifndef _WIN32
const int baud_rate = B115200;
#endif

static void set_port_options(int port)
{
#ifndef _WIN32
	struct termios options;

	// Get attributes from port device
	if (tcgetattr(port, &options) < 0)
		_LOG("Error on getting port attributes");

	// Configure port
	options.c_cflag |= CS8 | CREAD | CLOCAL; // CS8:    8bits per byte
	                                         // CLOCAL: No modem status info

	options.c_cc[VMIN]  = 0; // read without blocking
	options.c_cc[VTIME] = 5; // 0.5 sec timeout
	
	// Communication speed (baudrate)
	cfsetispeed(&options, baud_rate);
	cfsetospeed(&options, baud_rate);

	// Save attributes of port
	if (tcsetattr(port, TCSANOW, &options) < 0)
		_LOG("Error on setting port attributes");
#endif
}

static int open_port(const char* port_path)
{
#ifndef _WIN32
	// Open port as read-only and avoid port to control process
	int port = open(port_path, O_RDONLY | O_NOCTTY );
	if ( port == -1 ) {
		_LOG_V("Unable to open port", port_path);
	} else {
		fcntl(port, F_SETFL, 0);
		set_port_options(port);
	}

	return port;
#else
	return -1;
#endif
}

Ports sensor_setup(Sensor_Options& opts)
{
	Ports out_ports;

	if (opts.use_imu)
		out_ports.imu = open_port(opts.imu_port_path.c_str());

	if (opts.use_gnss)
		out_ports.gnss = open_port(opts.gnss_port_path.c_str());

	return out_ports;
}

const float us = 1e6; // Number of microseconds in a seconds

void flush_sensor_port(int port)
{
#ifndef _WIN32
	char        char_read;
	int         flush_count = 0;
	const int   max_flushes = 10;
	const int   flush_time  = 1;
	const float delta_t     = us*((float) flush_time)/((float) max_flushes);

	// Spend up to 1 second flushing the serial port
	while ( read(port, &char_read, 1) != 0 && flush_count < max_flushes) {
		tcflush(port, TCIOFLUSH);
		usleep(delta_t);
		flush_count++;

		printf("%c\n", char_read);
	}
	printf("End of flush.\n");

	// Removes imcomplete line, in case TCFLSH stops in the middle of a line
	do {
		if (read(port, &char_read, 1) != 0) {
			printf("Char read: %c\n", char_read);
		} else {
			printf("Nothing in port to read\n");
		}
	} while(char_read != '\n');
	printf("End of full flush.\n");
#endif
}

const int buffer_size = 256;
char buffer[buffer_size];

char* readl(int port)
{
	char char_read;
	int  index = 0;

	do {
		// Need to test read, since port is non-blocking
		//printf("reading char...\n");
		if (read(port, &char_read, 1) > 0) {
			buffer[index] = char_read;

			// Debug prints
			//if      (char_read == 0x0D) printf("<CR>\n");
			//else if (char_read == 0x0A) printf("<LF>\n");
			//else                        printf("%c\n", char_read);

			index++;
		} else {
			//printf("Nothing read in port\n");
			char_read = '\0';
		}
	} while (char_read != '\n' && index < buffer_size);

	// remove \r\n from string
	buffer[index-2] = '\0';

	return buffer;
}

const int chars_in_scalar = 32;
vector9d parse_imu (int port)
{
	vector9d meas;
	char char_read;

	//_LOG("Entered parse imu");

	// Remove message label: $INS
	do read(port, &char_read, 1); while(char_read != ',');

	char scalar_buffer[chars_in_scalar];
	for (int i = 0; i < 9; ++i) {

		int  index = 0;
		do {
			if (read(port, &char_read, 1) > 0) {
				//printf("%c", char_read);
				//if (char_read == '\n')
					//printf("<LF>");
				//if (char_read == '\r')
					//printf("<CR>");
				//printf("\n");
				scalar_buffer[index] = char_read;
				index++;
			//} else {
				//printf("Waiting...\n");
			}
		} while (char_read != ',' && char_read != '\r' && char_read != '$');

		if (char_read == '$') {
			do read(port, &char_read, 1); while(char_read != ',');
			i = 0;
			continue;
		}

		// Remove \r or ,
		scalar_buffer[index-1] = '\0';

		meas[i] = atof(scalar_buffer);
	}
	//printf("\n");

	//_LOG_9D("IMU MEAS", meas);

	return meas;
}

vector6d parse_gpgga(int port)
{
	vector6d meas;
	char char_read;

	// Remove UTC entry
	do read(port, &char_read, 1); while (char_read != ',');

	char scalar_buffer[chars_in_scalar];
	for (int i = 0; i < 2; ++i) {
		int index = 0;
	
		do {
			if(read(port, &char_read, 1) != 0) {
				scalar_buffer[index] = char_read;
				index++;
			}
		} while (char_read != ',' && char_read != '\r');
		scalar_buffer[index-1] = '\0';

		// Read cardinal sign
		do {} while (read(port, &char_read, 1) != 1);

		if (i == 0)
			meas[0] = lat_str_to_num(scalar_buffer, char_read);
		if (i == 1)
			meas[1] = lon_str_to_num(scalar_buffer, char_read);

		// Remove leading comma
		do {} while (read(port, &char_read, 1) != 1);
	}

	// Remove entries until height
	for (int i = 0; i < 4; ++i) {
		do {} while (read(port, &char_read, 1) != 1);
	}

	// Height
	int i = 2;
	int index = 0;
	do {
		if(read(port, &char_read, 1) != 0) {
			scalar_buffer[index] = char_read;
			index++;
		}
	} while (char_read != ',' && char_read != '\r');
	scalar_buffer[index-1] = '\0';
	meas[i] = atof(scalar_buffer);

	// read rest of line
	do { read(port, &char_read, 1); } while(char_read != '\n');

	return meas;
}

vector6d parse_gnss(int port)
{
	vector6d meas;
	char char_read;
	char label_buffer[8];

	// Remove message label: $GPGGA
	const char gps_label[] = "$GPGGA";
	while (1) {
		int i = 0;
		do {
			if ( read(port, &char_read, 1) != 0 ) {
				label_buffer[i] = char_read;
				i++;
			}
		} while(char_read != ',');
		label_buffer[i-1] = '\0';
		//printf("%s\n", label_buffer);

		// Check if msg label is correct = GPGGA
		if ( strcmp(label_buffer, gps_label) == 0 ) {
			//_LOG("GPGGA!");
			meas = parse_gpgga(port);
			break;
		} else {
			// Read rest of the line
			do{
				read(port, &char_read, 1);
			} while(char_read != '\n');
			read(port, &char_read, 1);
		}
	}

	return meas;
}

const scalar deg2rad = 0.0174532925199433;

scalar lat_str_to_num(char lat[32], char sign_lat)
{
	scalar rad;
	char deg[4];
	char minute[28];

	deg[0] = lat[0];
	deg[1] = lat[1];
	deg[2] = '\0';
	rad = atof(deg);

	for (int i = 0; i < 28; ++i) {
		minute[i] = lat[2+i];
		if (lat[2+i] == '\0') break;
	}
	rad += atof(minute)/60.0;

	if (sign_lat == 'S') rad = -rad;

	//_LOG_V("deg", rad);
	rad *= deg2rad;
	//_LOG_V("rad", rad);

	return rad;
}

scalar lon_str_to_num(char lon[32], char sign_lon)
{
	scalar rad;
	char deg[4];
	char minute[28];

	deg[0] = lon[0];
	deg[1] = lon[1];
	deg[2] = lon[2];
	deg[3] = '\0';
	rad = atof(deg);

	for (int i = 0; i < 28; ++i) {
		//_LOG_V("Lon_M_c", lon[3+i])
		minute[i] = lon[3+i];
		//if (lon[3+i] == '.') minute[i] = ',';
		if (lon[3+i] == '\0') break;
	}
	//_LOG_V("DEG_STR", deg);
	//_LOG_V("DEG_NUM", rad);
	//_LOG_V("MIN_STR", minute);
	//_LOG_V("MIN_NUM", atof(minute));
	//_LOG_V("MIN_NUM_IN_DEG", atof(minute)/60.0);
	rad += atof(minute)/60.0;
	//_LOG_V("ALL_NUM", rad);

	if (sign_lon == 'W') rad = -rad;

	// deg2rad
	//_LOG_V("deg", rad);
	rad *= deg2rad;
	//_LOG_V("RAD", rad);

	return rad;
}

void read_gpgga_from_cstr(char* line, char lat[32], char& sign_lat, 
		char lon[32], char& sign_lon, char h[32])
{
	//printf("line  = %s\n", line);

	char entry[32];
	int curr_idx = 0;
	int msg_idx = 0;
	//for (int i = 0; i < strlen(line.c_str()); ++i) {
	int line_idx = 0;
	while(line[line_idx] != '\0') {
		if (line[line_idx] == ',') {
			entry[curr_idx] = '\0';
			curr_idx = 0;
			printf("%s\n", entry);

			if (msg_idx == 2)
				memcpy(lat, entry, sizeof(entry));
			if (msg_idx == 3)
				sign_lat = entry[0];
			if (msg_idx == 4)
				memcpy(lon, entry, sizeof(entry));
			if (msg_idx == 5)
				sign_lon = entry[0];
			if (msg_idx == 10)
				memcpy(h, entry, sizeof(entry));

			msg_idx++;
		} else {
			printf("%c\n", line[line_idx]);
			entry[curr_idx] = line[line_idx];
			curr_idx++;
		}

		line_idx++;
	}
}
