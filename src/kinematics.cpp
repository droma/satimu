#include <kinematics.h>

Nav_State kinematics(Nav_State& curr, vector9d& imu_meas, vector6d gnss_meas, 
		scalar dt)
{
	/*
	 * Farrell2008 p388
	 */

	// Attitude kinematics
	vector4d q_b2e = curr.att;
	
	vector3d gyro;
	scalar   k       = 0.5*dt;
	scalar   rot_cal = datum_e_rot;
	vector3d e_rot_b = rot({{ 0, 0, rot_cal }}, conjugate(q_b2e));

	vector3d gyro_tmp;
	for (int i = 0; i < 3; ++i)
		//gyro[i] = imu_meas[i+3] - e_rot_b[i];
		//gyro_tmp[i] = imu_meas[i+3] - curr.gyro_b[i];
		gyro_tmp[i] = imu_meas[i+3];

	gyro[0] = (1 - curr.gyro_s[0])*gyro_tmp[0] + curr.gyro_m[0]*gyro_tmp[1] - curr.gyro_m[1]*gyro_tmp[2] - curr.gyro_b[0];
	gyro[1] = (1 - curr.gyro_s[1])*gyro_tmp[1] - curr.gyro_m[2]*gyro_tmp[0] + curr.gyro_m[3]*gyro_tmp[2] - curr.gyro_b[1];
	gyro[2] = (1 - curr.gyro_s[2])*gyro_tmp[2] + curr.gyro_m[4]*gyro_tmp[0] - curr.gyro_m[5]*gyro_tmp[1] - curr.gyro_b[2];

	for (int i = 0; i < 3; ++i)
		imu_meas[3+i] = gyro[i];

	for (int i = 0; i < 3; ++i)
		gyro[i] = e_rot_b[i] - gyro[i];
	
	q_b2e[0] += k*(  q_b2e[1]*gyro[0] + q_b2e[2]*gyro[1] + q_b2e[3]*gyro[2] );
	q_b2e[1] += k*( -q_b2e[0]*gyro[0] + q_b2e[3]*gyro[1] - q_b2e[2]*gyro[2] );
	q_b2e[2] += k*( -q_b2e[3]*gyro[0] - q_b2e[0]*gyro[1] + q_b2e[1]*gyro[2] );
	q_b2e[3] += k*(  q_b2e[2]*gyro[0] - q_b2e[1]*gyro[1] - q_b2e[0]*gyro[2] );
	q_b2e = conv_to_unit(q_b2e);

	curr.att = q_b2e;

	// Velocity kinematics
	vector3d vel = curr.vel;
	vector3d acc;
	vector3d llh = ecef2llh(curr.pos);
	_LOG_3D("LLH", llh);
	vector3d grav = grav_model_ecef(llh);

	acc[0] = (1 - curr.acc_s[0])*imu_meas[0] + curr.acc_m[0]*imu_meas[1] - curr.acc_m[1]*imu_meas[2] - curr.acc_b[0];
	acc[1] = (1 - curr.acc_s[1])*imu_meas[1] - curr.acc_m[2]*imu_meas[0] + curr.acc_m[3]*imu_meas[2] - curr.acc_b[1];
	acc[2] = (1 - curr.acc_s[2])*imu_meas[2] + curr.acc_m[4]*imu_meas[0] - curr.acc_m[5]*imu_meas[1] - curr.acc_b[2];

	for (int i = 0; i < 3; ++i)
		imu_meas[i] = acc[i];

	vector3d res;
	//_LOG_4D("Q_e2b", q_b2e);
	res = rot(acc, q_b2e);

	vector3d ro;
	ro[0] =  2*datum_e_rot*vel[1];
	ro[1] = -2*datum_e_rot*vel[0];
	ro[2] =  0;

	vel[0] += dt*(res[0] - grav[0] + ro[0]);
	vel[1] += dt*(res[1] - grav[1] + ro[1]);
	vel[2] += dt*(res[2] - grav[2] + ro[2]);

	//imu_meas[0] = res[0] - grav[0];
	//imu_meas[1] = res[1] - grav[1];
	//imu_meas[2] = res[2] - grav[2];

	_LOG_3D("Acc^e", res);
	_LOG_V("|Acc^e|", get_norm(res));
	_LOG_3D("Grav^e", grav);
	_LOG_V("|Grav^e|", get_norm(grav));
	_LOG_3D("Rot term", ro);

	curr.vel = vel;

	// Position kinematics
	curr.pos[0] += dt*curr.vel[0]; // - datum_e_rot*curr.pos[1];
	curr.pos[1] += dt*curr.vel[1]; // + datum_e_rot*curr.pos[0];
	curr.pos[2] += dt*curr.vel[2];

	return curr;
}
