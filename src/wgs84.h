#ifndef __WGS84_H__
#define __WGS84_H__

// Source: http://earth-info.nga.mil/GandG/publications/tr8350.2/wgs84fin.pdf

const double datum_gm      = 3.986004418e14;   // Grav_const*Mass 
const double datum_a       = 6378137.0;
const double datum_f       = 1/298.257223563;
const double datum_1of     = 298.25722356;
const double datum_b       = 6356752.3;
const double datum_e       = 0.0818192;
const double datum_e2      = 6.69437999014e-3;
const double datum_el      = 0.0820944;
const double datum_el2     = 6.73949674228e-3;
const double datum_alfa    = 4.69314;          // [deg]
const double datum_ae      = 521854.0;
const double datum_gamma_e = 9.7803253359;
const double datum_k       = 0.00193185265241;
const double datum_e_rot   = 7.292115e-5;      // Earth rotation [rad/s]

#endif
