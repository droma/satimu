#ifndef __NAV_STATE_H__
#define __NAV_STATE_H__

#include <utils/algebra.h>

typedef struct {
	vector3d pos {{ 0, 0, 0 }};
	vector3d vel {{ 0, 0, 0 }};
	vector4d att {{ 1, 0, 0, 0 }};

	//vector3d acc_b  {{ 0.2, -0.1, 0.1 }};
	vector3d acc_b  {{ 0, 0, 0 }};
	vector3d acc_s  {{ 0, 0, 0 }};
	vector6d acc_m  {{ 0, 0, 0, 0, 0, 0 }};

	vector3d gyro_b  {{ 0, 0, 0 }};
	vector3d gyro_s  {{ 0, 0, 0 }};
	vector6d gyro_m  {{ 0, 0, 0, 0, 0, 0 }};
} Nav_State;

void init_nav_state(Nav_State& nav_state);

#endif
