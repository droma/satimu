CC = ccache g++
# C++ flags
CFLAGS = -g -O1 -g -Wall -pedantic -march=native -std=c++17
# Loader flags
LDBOOST = -lboost_iostreams -lboost_system -lboost_filesystem
LDFLAGS = -lm -pthread $(LDBOOST)

# Remove main.o to avoid conflicts of int main function
SRC_DIR = ../src
OBJ_DIR = ../build
SRC_OBJ = $(shell find $(OBJ_DIR) -type f -name "*.o" -and -not -name "main.o")
TEST_SRC_DIR = ./test_cases
TEST_OBJ_DIR = ./test_build
TEST_BIN_DIR = ./test_bin
TEST_SRC = $(shell find $(TEST_SRC_DIR) -type f -name "*.cpp")
TEST_OBJ = $(foreach obj, $(TEST_SRC:.cpp=.o), $(TEST_OBJ_DIR)/$(shell basename $(obj)) )
TEST_BIN = $(foreach obj, $(TEST_SRC:.cpp=),   $(TEST_BIN_DIR)/$(shell basename $(obj)) )

all: $(TEST_BIN) $(TEST_OBJ)

$(TEST_OBJ_DIR):
	@mkdir $(TEST_OBJ_DIR)

$(TEST_BIN_DIR):
	@mkdir $(TEST_BIN_DIR)

$(TEST_BIN_DIR)/%: $(TEST_OBJ_DIR)/%.o $(SRC_OBJ) | $(TEST_BIN_DIR)
	@echo -e "[EXE]" $@
	@$(CC) $(LDFLAGS) -I$(SRC_DIR) $^ -o $@

$(TEST_OBJ_DIR)/%.o: $(TEST_SRC_DIR)/%.cpp ./tester.h | $(TEST_OBJ_DIR)
	@echo -e "[CPP]" $<
	@$(CC) $(CFLAGS) -I$(SRC_DIR) -c $< -o $@

.PHONY: clean echo

clean:
	rm -rf test_build
	rm -rf test_bin

echo:
	@echo $(TEST_BIN)
