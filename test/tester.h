#include <string>
#include <string.h>

struct Tester {
	int   err_num = 0;
	char* test_name;

	Tester(std::string test_name)
	{
		this->test_name = (char*)malloc(test_name.length());
		strcpy(this->test_name, test_name.c_str());
		printf("%s\n", this->test_name);
	}

	void assert_test(bool test, std::string fail_msg)
	{
		if (test) {
			printf("[%s] passed\n", test_name);
			return;
		}

		printf(" >>> [%s] failed\n", test_name);
		printf(" >>>>>>>>>>>>>>>>>>>>>>>> %s\n", fail_msg.c_str());
		err_num++;
	}

	void finish()
	{
		free(this->test_name);

		if (err_num)
			printf("\x1b[31mNumber of errors: %i\x1b[0m\n\n", err_num);
		else
			printf("\x1b[32mNo errors\x1b[0m\n\n");
	}
};
