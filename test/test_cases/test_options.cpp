#include <../test/tester.h>
#include <options.h>

#define APP_PATH "./SatIMU"

Tester tester("Command argument parser");

void test_cmd_flag_opts()
{
	const char* argv[] = {APP_PATH, "--text_mode", "--draw_pos"};
	int         argc   = sizeof(argv) / sizeof(char*);

	Options* opt = new Options(argc, argv);
	opt->parse();

	tester.assert_test(opt->std_out_opts.verbose == true,
	                   "Verbose was not set");
	tester.assert_test(opt->std_out_opts.text_mode == true,
	                   "Text mode was not set");
	tester.assert_test(opt->std_out_opts.display_in_enu == false,
	                   "Display in ENU was not set");

	delete opt;
}

int main(void)
{
	test_cmd_flag_opts();
	tester.finish();

	return 0;
}
