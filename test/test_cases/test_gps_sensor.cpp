#include <utils/algebra.h>
#include <sensors/sensors.h>
#include <../test/tester.h>

Tester tester("GPS sensor parser");

void test_gps()
{
	// Test nmea msg
	std::string line = "$GPGGA,13.40,3844.3285,N,00909.4698,E,,,,,50.7,M,,,*00";

	char cline[256];
	strcpy(cline, line.c_str());

	char lat[16], lon[16], sign_lat, sign_lon, h[16];
	read_gpgga_from_cstr(cline, lat, sign_lat, lon, sign_lon, h);

	vector3d meas;
	meas[0] = lat_str_to_num(lat, sign_lat);
	//_LOG(lon);
	meas[1] = lon_str_to_num(lon, sign_lon);
	meas[2] = atof(h);

	//_LOG_3D("MEAS", meas);

	tester.assert_test( std::abs(meas[0] - 38.73881) < 0.0001, "Lat is wrong.");
	tester.assert_test( std::abs(meas[1] - 9.15783)  < 0.0001, "Lon is wrong.");
	tester.assert_test( std::abs(meas[2] - 50.7)     < 0.0001, "Height is wrong.");
}

int main(void)
{
	test_gps();
	tester.finish();

	return 0;
}
