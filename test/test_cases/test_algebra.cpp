#include <../test/tester.h>
#include <utils/algebra.h>
#include <utils/log.h>

Tester tester("Algebra");

void test_inv_6x6_cov()
{
	matrix6d a {{ {{  2, -1,  0,  0,  0,  0 }},
		          {{ -1,  2, -1,  0,  0,  0 }},
		          {{  0, -1,  2, -1,  0,  0 }},
		          {{  0,  0, -1,  2, -1,  0 }},
		          {{  0,  0,  0, -1,  2, -1 }},
		          {{  0,  0,  0, 0,  -1,  2 }} }};

	matrix6d b = inv6d_sim_pd(a);

	//for (int i = 0; i < 5; ++i)
		//_LOG_5D("INV(A)", b[i]);

	tester.assert_test(std::abs(b[0][0] - 0.8333) < 0.001,
	                   "Rnd value 1 doesn't check out");
	tester.assert_test(std::abs(b[0][1] - 0.6667) < 0.001,
	                   "Rnd value 2 doesn't checks out");
	tester.assert_test(std::abs(b[1][0] - 0.6667) < 0.001,
	                   "Rnd value 3 doesn't checks out");
	tester.assert_test(std::abs(b[2][3] - 1.0000) < 0.001,
	                   "Rnd value 4 doesn't checks out");
	tester.assert_test(std::abs(b[4][2] - 0.5000) < 0.001,
	                   "Rnd value 5 doesn't checks out");
}

int main(void)
{
	test_inv_6x6_cov();
	tester.finish();

	return 0;
}
