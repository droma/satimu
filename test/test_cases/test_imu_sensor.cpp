#include <sensors/sensors.h>
#include <../test/tester.h>
#include <array>
#include <utils/algebra.h>

Tester tester("IMU sensor parser");

bool cmp_3f_to_f(float vec1, float vec2, float vec3, float vec_k)
{
	if (vec1 == vec_k) {
		if (vec2 == vec_k) {
			if (vec3 == vec_k) return true;
		}
	}

	return false;
}

void test_imu()
{
	// Open tty tunnel
	const std::string imu_port_path = "./pty.out";
	const std::string in_port_path = "./pty.in";
	char socat_cmd[256];
	sprintf(socat_cmd,"socat PTY,link=%s,rawer PTY,link=%s,rawer &",
			in_port_path.c_str(), imu_port_path.c_str());
	system(socat_cmd);

	// Open input port
	int in_port = open(in_port_path.c_str(), O_WRONLY | O_NOCTTY);
	if (in_port < 0) {
		_LOG("Can't open input serial port");
		return;
	}
	fcntl(in_port, F_SETFL, 0);

	// Open output port
	Sensor_Options sen_opts = {"", true, false, imu_port_path};
	Ports p = sensor_setup(sen_opts);

	// Send message
	float num1 = 0.111111;
	float num2 = -0.111111;
	float num3 = -120.111111;
	char msg[256];
	sprintf(msg,"$INS,%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n", num1, num2, num3,
													   num1, num2, num3,
													   num1, num2, num3);

	write(in_port, msg, 256);

	// Parse message
	vector9d meas;
	meas = parse_imu(p.imu);

	tester.assert_test(cmp_3f_to_f(meas[0], meas[3], meas[6], num1),
			"Normal float in not parsed correctely");
	tester.assert_test(cmp_3f_to_f(meas[1], meas[4], meas[7], num2),
			"Negative float in not parsed correctely");
	tester.assert_test(cmp_3f_to_f(meas[2], meas[5], meas[8], num3),
			"High float in not parsed correctely");
}

int main(void)
{
	test_imu();
	tester.finish();

	return 0;
}
