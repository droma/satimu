cmake_minimum_required(VERSION 2.8)
set(proj_name SatIMU)
project(${proj_name})

# Usage (WIN32):
#   cmake -DBOOST_ROOT="<boost-path>" ..

# Boost
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

if (WIN32)
	set(Boost_USE_STATIC_LIBS        ON)  # only find static libs
	set(Boost_USE_MULTITHREADED      ON)
	set(Boost_USE_STATIC_RUNTIME    OFF)

	#set(BOOST_ROOT            "./boost")
	#set(BOOST_LIBRARYDIR      "./boost/lib")
	#set(BOOST_INCLUDEDIR      "./boost/include")
	#set(Boost_NO_SYSTEM_PATHS ON)
endif()

find_package(Boost COMPONENTS filesystem system iostreams REQUIRED)
#include_directories(${BOOST_ROOT})
include_directories(${Boost_INCLUDE_DIRS})

message(STATUS "Boost root:        ${BOOST_ROOT}")
message(STATUS "Boost include dir: ${Boost_INCLUDE_DIRS}")
message(STATUS "Boost library dir: ${Boost_LIBRARY_DIRS}")
message(STATUS "Boost libraries:   ${Boost_LIBRARIES}")

set(src_dir "${CMAKE_SOURCE_DIR}/src")
include_directories(${src_dir})

add_library(aug_ekf       ${src_dir}/aug_ekf.cpp)
add_library(aug_ukf       ${src_dir}/aug_ukf.cpp)
add_library(aug_kf_common ${src_dir}/aug_kf_common.cpp)
add_library(ekf           ${src_dir}/ekf.cpp)
add_library(ukf           ${src_dir}/ukf.cpp)
add_library(kf_common     ${src_dir}/kf_common.cpp)
add_library(kinematics    ${src_dir}/kinematics.cpp)
add_library(nav_state     ${src_dir}/nav_state.cpp)
add_library(options       ${src_dir}/options.cpp)
add_library(cmd_parser    ${src_dir}/opts/cmd_parser.cpp)
add_library(config_parser ${src_dir}/opts/config_parser.cpp)
add_library(file_mode     ${src_dir}/sensors/file_mode.cpp)
add_library(sensors       ${src_dir}/sensors/sensors.cpp)
add_library(algebra       ${src_dir}/utils/algebra.cpp)
add_library(grav_model    ${src_dir}/grav_model.cpp)
add_library(alignment     ${src_dir}/alignment.cpp)

add_executable(${proj_name} ${src_dir}/main.cpp)
target_link_libraries(${proj_name} aug_ekf aug_ukf aug_kf_common ekf ukf kf_common kinematics nav_state options cmd_parser config_parser file_mode sensors algebra grav_model alignment ${Boost_LIBRARIES})

#file(COPY conf.ini DESTINATION .)
file(COPY conf.ini DESTINATION ${CMAKE_BINARY_DIR})
