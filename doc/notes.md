# Project Notes

To find to-dos, bugs and code to improved, search for `@Todo`

## File Structure

   * `src/` [includes all application source code]
      * `main.cpp`
      * `options/` [option struct and routine to parse options]
         * `options.h`
         * `cmd_opts_parser.h`
         * `conf_opts_parser.h`
      * `sensors/` [controls sensors connected to serial]
         * `smu.h`
         * `imu.h`
         * `sensors.h`
      * `plotter/` [plot values, mainly for debug]
         * `plotter.h`
      * `nav_sys/` [navigation system]
         * `kinematics.h`
         * `kalman_filter.h`
         * `wgs84.h` [no runtime logic, just defines WGS 84 constants]
      * `utils/` [misc code]
         * `log.h`
   * `test/` [everything referring to tests]
      * `tester.h`
      * `test_cases/` [all test case code]
         * `test_options.cpp`
         * `test_sensors.cpp`
         * `test_plotter.cpp`
         * `test_kinematics.cpp`
         * `test_kalman_filter.cpp`
      * `test_bin/` [all compiled code from `test_cases/`]
         * ...
   * `build/` [object files from compilation of `src` folder]
      * `<same folder structure as src/>`
   * `bin/` [application executables]
      * `debug/`
      * `release/`

## Binaries

### Debug

   * Optimization flags: `-O1` (faster to compile)
   * Extra compilation flags: `-g -DDEBUG`
   * All plotters enabled by default
   * Error handling disabled (but warnings should be made though `utils/log.h`)

### Release

   * Optimization flags: `-O2`
   * All plotters disabled by default
   * Error handling enabled

## Tasks

   * Timer class to sleep loop
      * Read options
      * Test various cases
   * Log library
   * Test library
   * Option parser
      * Read configuration file
      * Parse command line arguments
   * Sensor wrapper
   * Navigation System
   * Create debug mode exclusively for testing
      * Should not have any error handling
      * Should be the only mode with plotters
      * Problem, how to have both Debug and Release code use `conf.ini` ? 
         * Use `config_path` commandline option?

## Issues

### Options

   * Options should be an object structure, since what is valued from the optioned structure is it's flexibility towards the addition of new class without having to modify existing functions.

      // application.cpp
      opts->opts_parser.parse();

      // opts_parser.cpp
      Option opts;
      Config_Parser conf_parser;
      CMD_Flag_Parser cmd_flg_parser;

      bool parse_config = cmd_flg_parser.check_if_config_to_parse();
      if (parse_config) {
         std::string config_path = cmd_flg_parser.get_config_path();

         opts = conf_parser.parse();
         opts = 
      }

   * Properties in `ptree` should be checked before writing to variable
      * A solution is to test for the existence of the process and throw an exception when the test fails. There is an issue with the fact that a non-void returning function may exit without a return value.

   * There shouldn't be indications of the unit in variable names when these are in SI. In fact, all variable names should use common naming conventions, ex.: freq for frequency. Yet, the configuration file should use more pedantic names, as to provide greater accessibility.

   * Commandline flags should have two extra options:
      * `--use_config`: if active, use default values
      * `--config_path=<path_to_config>`: to use specify a different configuration path

   * Problems arise when parse is run twice. The issue seems to lie in GNU's `getopts` function.

