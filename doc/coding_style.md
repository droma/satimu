# Coding Style

Mostly influenced by the Linux kernel coding style (lkcs). Differences will be noted.

## General

   * Indentations are tabs with a width of 4 spaces (different from lkcs).
      * Every code block encapsulated by a higher level statement (ex.: `if`, `for`, function definition) should be indented. Except for the `cases`, notice how each `case` statement is on the same level as the `switch` statement:

      switch (var_to_test) {
      case op1:
         // Code
      case op2:
         // Code
         // ...

   * Lines should not go past 80 chars.
      * Splitting a code-line into two lines (ex.: leaving last argument of a function in the following line) is accepted but not recommended.

## Brace Placement

   * Curly brackets (braces) placement depends on the situation
      * For functions, structs and classes, use the following

      some_type some_func( /* args */ )
      {
         // Code...
      }

      * Whereas loops, conditions and any other code block should be use the following:

      if ( /* condition */ ) {
         // Code...
      }

## Naming 

   * All names should be composed by adequate words or abbreviations separated by underscores
   * Variable names should be in all lowercase
   * Class and struct typdefs are capitalized in the first character and after each underscore
   * Defines should be in all uppercase

