# Commandline options

   * `draw_pos/draw_vel/draw_att` activates plotter for that variable
      * Only works if `text_mode` is active in configuration
   * `text_mode` disables all graphical elements (plotters)
   * `plot_term`
   * `plot_interval`
   * `plot_freq`
