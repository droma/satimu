function [OSx, OSy, OSz, SCx, SCy, SCz] = Ellipsoid_to_Sphere(data, graph_filename)
  
  disp(data(:,1))
  SpherePlot(data(:,1), data(:,2), data(:,3), 'Uncalibrated data', strcat(graph_filename, '_uncalibrated.pdf'));

  %X=pinv([data(:,1) data(:,2) data(:,3) -data(:,2).^2 -data(:,3).^2 ones(size(data,1),1)])*(data(:,1).^2);
  %[data(:,2) -data(:,2).^2 -(data(:,2).^2)]
  H = [data(:,1) data(:,2) data(:, 3) -data(:,2).^2 -data(:,3).^2 ones(size(data,1),1)];
  w = data(:,1).^2;
  X = H \ w;
  OSx=X(1)/2;
  OSy=X(2)/(2*X(4));
  OSz=X(3)/(2*X(5));
  A=X(6)+OSx^2+X(4)*OSy^2+X(5)*OSz^2;
  B=A/X(4);
  C=A/X(5);
  SCx=sqrt(A);
  SCy=sqrt(B);
  SCz=sqrt(C);

  xx=data(:,1)-OSx;
  yy=data(:,2)-OSy;
  zz=data(:,3)-OSz;

  xxx=xx./SCx;
  yyy=yy./SCy;
  zzz=zz./SCz;

  SpherePlot(xxx, yyy, zzz, 'Calibrated data', strcat(graph_filename, '_calibrated.pdf'));
  
end