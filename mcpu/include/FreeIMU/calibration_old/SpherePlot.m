function SpherePlot(x, y, z, graph_title, filename)	
  figure;
  subplot(2,2,1); plot3(x, y, z);
  title(graph_title);
  subplot(2,2,2); plot(x, y);
  title('Vector projection on xy');
  subplot(2,2,3); plot(y, z);
  title('Vector projection on yz');
  subplot(2,2,4); plot(z, x);
  title('Vector projection on zx');
  print(filename) % .pdf graphs generated
end