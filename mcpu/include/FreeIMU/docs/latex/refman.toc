\contentsline {chapter}{\numberline {1}Data Structure Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Data Structures}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}File Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}File List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Data Structure Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Free\discretionary {-}{}{}I\discretionary {-}{}{}M\discretionary {-}{}{}U Class Reference}{5}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Constructor \& Destructor Documentation}{5}{subsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.1}Free\discretionary {-}{}{}I\discretionary {-}{}{}M\discretionary {-}{}{}U}{5}{subsubsection.3.1.1.1}
\contentsline {subsection}{\numberline {3.1.2}Member Function Documentation}{5}{subsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.2.1}get\discretionary {-}{}{}Euler}{5}{subsubsection.3.1.2.1}
\contentsline {subsubsection}{\numberline {3.1.2.2}get\discretionary {-}{}{}Euler\discretionary {-}{}{}Rad}{6}{subsubsection.3.1.2.2}
\contentsline {subsubsection}{\numberline {3.1.2.3}get\discretionary {-}{}{}Q}{6}{subsubsection.3.1.2.3}
\contentsline {subsubsection}{\numberline {3.1.2.4}get\discretionary {-}{}{}Raw\discretionary {-}{}{}Values}{6}{subsubsection.3.1.2.4}
\contentsline {subsubsection}{\numberline {3.1.2.5}get\discretionary {-}{}{}Values}{6}{subsubsection.3.1.2.5}
\contentsline {subsubsection}{\numberline {3.1.2.6}get\discretionary {-}{}{}Yaw\discretionary {-}{}{}Pitch\discretionary {-}{}{}Roll}{6}{subsubsection.3.1.2.6}
\contentsline {subsubsection}{\numberline {3.1.2.7}get\discretionary {-}{}{}Yaw\discretionary {-}{}{}Pitch\discretionary {-}{}{}Roll\discretionary {-}{}{}Rad}{6}{subsubsection.3.1.2.7}
\contentsline {subsubsection}{\numberline {3.1.2.8}init}{7}{subsubsection.3.1.2.8}
\contentsline {subsubsection}{\numberline {3.1.2.9}init}{7}{subsubsection.3.1.2.9}
\contentsline {subsubsection}{\numberline {3.1.2.10}init}{7}{subsubsection.3.1.2.10}
\contentsline {subsection}{\numberline {3.1.3}Field Documentation}{7}{subsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.3.1}acc}{7}{subsubsection.3.1.3.1}
\contentsline {subsubsection}{\numberline {3.1.3.2}baro}{7}{subsubsection.3.1.3.2}
\contentsline {subsubsection}{\numberline {3.1.3.3}gyro}{7}{subsubsection.3.1.3.3}
\contentsline {subsubsection}{\numberline {3.1.3.4}magn}{7}{subsubsection.3.1.3.4}
\contentsline {subsubsection}{\numberline {3.1.3.5}raw\discretionary {-}{}{}\_\discretionary {-}{}{}acc}{7}{subsubsection.3.1.3.5}
\contentsline {subsubsection}{\numberline {3.1.3.6}raw\discretionary {-}{}{}\_\discretionary {-}{}{}gyro}{7}{subsubsection.3.1.3.6}
\contentsline {subsubsection}{\numberline {3.1.3.7}raw\discretionary {-}{}{}\_\discretionary {-}{}{}magn}{7}{subsubsection.3.1.3.7}
\contentsline {chapter}{\numberline {4}File Documentation}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}Communication\discretionary {-}{}{}Utils.\discretionary {-}{}{}cpp File Reference}{9}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Function Documentation}{9}{subsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.1}serial\discretionary {-}{}{}Float\discretionary {-}{}{}Print}{9}{subsubsection.4.1.1.1}
\contentsline {subsubsection}{\numberline {4.1.1.2}serial\discretionary {-}{}{}Print\discretionary {-}{}{}Float\discretionary {-}{}{}Arr}{9}{subsubsection.4.1.1.2}
\contentsline {subsubsection}{\numberline {4.1.1.3}write\discretionary {-}{}{}Arr}{9}{subsubsection.4.1.1.3}
\contentsline {subsubsection}{\numberline {4.1.1.4}write\discretionary {-}{}{}Var}{9}{subsubsection.4.1.1.4}
\contentsline {section}{\numberline {4.2}Communication\discretionary {-}{}{}Utils.\discretionary {-}{}{}h File Reference}{9}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Function Documentation}{10}{subsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.1.1}serial\discretionary {-}{}{}Float\discretionary {-}{}{}Print}{10}{subsubsection.4.2.1.1}
\contentsline {subsubsection}{\numberline {4.2.1.2}serial\discretionary {-}{}{}Print\discretionary {-}{}{}Float\discretionary {-}{}{}Arr}{10}{subsubsection.4.2.1.2}
\contentsline {subsubsection}{\numberline {4.2.1.3}write\discretionary {-}{}{}Arr}{10}{subsubsection.4.2.1.3}
\contentsline {subsubsection}{\numberline {4.2.1.4}write\discretionary {-}{}{}Var}{10}{subsubsection.4.2.1.4}
\contentsline {section}{\numberline {4.3}Free\discretionary {-}{}{}I\discretionary {-}{}{}M\discretionary {-}{}{}U.\discretionary {-}{}{}cpp File Reference}{10}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Function Documentation}{10}{subsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.1.1}arr3\discretionary {-}{}{}\_\discretionary {-}{}{}rad\discretionary {-}{}{}\_\discretionary {-}{}{}to\discretionary {-}{}{}\_\discretionary {-}{}{}deg}{10}{subsubsection.4.3.1.1}
\contentsline {subsubsection}{\numberline {4.3.1.2}inv\discretionary {-}{}{}Sqrt}{10}{subsubsection.4.3.1.2}
\contentsline {section}{\numberline {4.4}Free\discretionary {-}{}{}I\discretionary {-}{}{}M\discretionary {-}{}{}U.\discretionary {-}{}{}h File Reference}{10}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Macro Definition Documentation}{11}{subsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.1.1}cbi}{11}{subsubsection.4.4.1.1}
\contentsline {subsubsection}{\numberline {4.4.1.2}F\discretionary {-}{}{}I\discretionary {-}{}{}M\discretionary {-}{}{}U\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}A\discretionary {-}{}{}C\discretionary {-}{}{}C\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}D\discretionary {-}{}{}R}{11}{subsubsection.4.4.1.2}
\contentsline {subsubsection}{\numberline {4.4.1.3}F\discretionary {-}{}{}I\discretionary {-}{}{}M\discretionary {-}{}{}U\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}B\discretionary {-}{}{}A\discretionary {-}{}{}R\discretionary {-}{}{}O\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}D\discretionary {-}{}{}R}{11}{subsubsection.4.4.1.3}
\contentsline {subsubsection}{\numberline {4.4.1.4}F\discretionary {-}{}{}I\discretionary {-}{}{}M\discretionary {-}{}{}U\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}B\discretionary {-}{}{}M\discretionary {-}{}{}A180\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}D\discretionary {-}{}{}E\discretionary {-}{}{}F\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}D\discretionary {-}{}{}R}{11}{subsubsection.4.4.1.4}
\contentsline {subsubsection}{\numberline {4.4.1.5}F\discretionary {-}{}{}I\discretionary {-}{}{}M\discretionary {-}{}{}U\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}I\discretionary {-}{}{}T\discretionary {-}{}{}G3200\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}D\discretionary {-}{}{}E\discretionary {-}{}{}F\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}D\discretionary {-}{}{}R}{11}{subsubsection.4.4.1.5}
\contentsline {subsubsection}{\numberline {4.4.1.6}F\discretionary {-}{}{}R\discretionary {-}{}{}E\discretionary {-}{}{}E\discretionary {-}{}{}I\discretionary {-}{}{}M\discretionary {-}{}{}U\discretionary {-}{}{}\_\discretionary {-}{}{}v04}{11}{subsubsection.4.4.1.6}
\contentsline {subsubsection}{\numberline {4.4.1.7}H\discretionary {-}{}{}A\discretionary {-}{}{}S\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}A\discretionary {-}{}{}D\discretionary {-}{}{}X\discretionary {-}{}{}L345}{11}{subsubsection.4.4.1.7}
\contentsline {subsubsection}{\numberline {4.4.1.8}H\discretionary {-}{}{}A\discretionary {-}{}{}S\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}A\discretionary {-}{}{}X\discretionary {-}{}{}I\discretionary {-}{}{}S\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}A\discretionary {-}{}{}L\discretionary {-}{}{}I\discretionary {-}{}{}G\discretionary {-}{}{}N\discretionary {-}{}{}E\discretionary {-}{}{}D}{11}{subsubsection.4.4.1.8}
\contentsline {subsubsection}{\numberline {4.4.1.9}H\discretionary {-}{}{}A\discretionary {-}{}{}S\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}B\discretionary {-}{}{}M\discretionary {-}{}{}A180}{12}{subsubsection.4.4.1.9}
\contentsline {subsubsection}{\numberline {4.4.1.10}H\discretionary {-}{}{}A\discretionary {-}{}{}S\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}H\discretionary {-}{}{}M\discretionary {-}{}{}C5883\discretionary {-}{}{}L}{12}{subsubsection.4.4.1.10}
\contentsline {subsubsection}{\numberline {4.4.1.11}H\discretionary {-}{}{}A\discretionary {-}{}{}S\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}I\discretionary {-}{}{}T\discretionary {-}{}{}G3200}{12}{subsubsection.4.4.1.11}
\contentsline {subsubsection}{\numberline {4.4.1.12}H\discretionary {-}{}{}A\discretionary {-}{}{}S\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}M\discretionary {-}{}{}P\discretionary {-}{}{}U6050}{12}{subsubsection.4.4.1.12}
\contentsline {subsubsection}{\numberline {4.4.1.13}H\discretionary {-}{}{}A\discretionary {-}{}{}S\discretionary {-}{}{}\_\discretionary {-}{}{}\discretionary {-}{}{}M\discretionary {-}{}{}S5611}{12}{subsubsection.4.4.1.13}
\contentsline {subsubsection}{\numberline {4.4.1.14}I\discretionary {-}{}{}S\discretionary {-}{}{}\_\discretionary {-}{}{}6\discretionary {-}{}{}D\discretionary {-}{}{}O\discretionary {-}{}{}M}{12}{subsubsection.4.4.1.14}
\contentsline {subsubsection}{\numberline {4.4.1.15}I\discretionary {-}{}{}S\discretionary {-}{}{}\_\discretionary {-}{}{}9\discretionary {-}{}{}D\discretionary {-}{}{}O\discretionary {-}{}{}M}{12}{subsubsection.4.4.1.15}
\contentsline {subsubsection}{\numberline {4.4.1.16}two\discretionary {-}{}{}Ki\discretionary {-}{}{}Def}{12}{subsubsection.4.4.1.16}
\contentsline {subsubsection}{\numberline {4.4.1.17}two\discretionary {-}{}{}Kp\discretionary {-}{}{}Def}{12}{subsubsection.4.4.1.17}
\contentsline {subsection}{\numberline {4.4.2}Function Documentation}{12}{subsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.2.1}arr3\discretionary {-}{}{}\_\discretionary {-}{}{}rad\discretionary {-}{}{}\_\discretionary {-}{}{}to\discretionary {-}{}{}\_\discretionary {-}{}{}deg}{12}{subsubsection.4.4.2.1}
\contentsline {subsubsection}{\numberline {4.4.2.2}inv\discretionary {-}{}{}Sqrt}{12}{subsubsection.4.4.2.2}
