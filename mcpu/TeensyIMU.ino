// FreeSizIMU library
#include <FreeIMU.h>
#include <ADXL345.h>
#include <ITG3200.h>
#include <I2Cdev.h>
#include <HMC58X3.h>
#include <MPU60X0.h>
//#include <MS561101BA.h>
#include <SPI.h>
#include <EEPROM.h>
#include <DebugUtils.h>

#include <EEPROM.h>

#include <Wire.h>
#include <Adafruit_GFX.h>
/*#include <Adafruit_SSD1306.h>*/
#include <OLED_64H_SSD1306.h>
/*#include <OLED_32H_SSD1306.h>*/

#include <SD.h>

#define BAUD      115200
#define FREQ      100     // Sample frequency
#define SEC2MICRO 1e6
#define SEC2MILI  1e3

#define OLED_RESET 4
#define MAX_CHAR   9
#define OLED_FREQ  1

#define DEG2RAD 0.0174532925199433
#define GRAV    9.80665

/*#define DEBUG*/

// Set the FreeSixIMU object
FreeIMU imu = FreeIMU();

Adafruit_SSD1306 display(OLED_RESET);

// Raw input variables
/*float a_values[3], g_values[3];*/
float values[9];

float t = 0;
float prev_time;
float over_time;
float time_step  = 1.0/FREQ;
float oled_tstep = 1.0/OLED_FREQ;
float delta_t;
int it = 0;

bool _save2sd = true;
File myFile;

// initializes all values of buffer
void init_disp_buf(char* buf, int n)
{
    for (int i = 0; i < n; i++) {
    	buf[i] = 0;
    }
}

void display_array(const char* buf, int n)
{
	
	for (int i = 0; i < n; i++) {
		display.write(buf[i]);
	}
}

void display_float(float num)
{
    char buf[MAX_CHAR];

	init_disp_buf(buf, MAX_CHAR);

    sprintf(buf, "%f", num);
    display_array(buf, MAX_CHAR);
}

void display_block(const char* label, int max_label, float* val, int max_vals,
	int x, int y)
{
    display.setCursor(x, y);
    display_array(label, max_label);

	for (int i = 0; i < max_vals; i++) {
    	display.setCursor(x, y + 8 + 8*i);
    	display_float(val[i]);
	}
}

float oled_time, oled_dt;
float oled_ptime = 0;

void display_info() 
{
	Serial.print("$INS");
	for (int i = 0; i < 9; i++) {
		Serial.print(',');
		Serial.print(values[i], DEC);
	}
	Serial.print("\r\n");

	if (_save2sd) {
		myFile = SD.open("file.dat", FILE_WRITE);
		myFile.print("$INS");
		for (int i = 0; i < 9; i++) {
			myFile.print(',');
			myFile.print(values[i], DEC);
		}
		myFile.print("\r\n");
		myFile.close();
	}

	oled_time = micros();
	oled_dt = (oled_time - oled_ptime)/SEC2MICRO;
	if ( oled_dt > oled_tstep ) {
		/*float t_slept = (micros() - prev_time)/SEC2MICRO;*/

		display.clearDisplay();

		display_block("---Acce---", 10, &values[0], 3, 0, 0);
		display_block("---Gyro---", 10, &values[3], 3, 64, 0);
		display_block("---Magn---", 10, &values[6], 3, 0, 32);
		display_block("---Step---", 10, &delta_t, 1, 64, 32);
		/*display_block("TSlept:", 7, &t_slept, 1, 64, 48);*/

		display.println();
		display.display();
	
		oled_ptime = oled_time;
	}

}

void setup() {
	Serial.begin(BAUD);
	Wire.begin();

	#ifdef DEBUG
		Serial.println("Initializing IMU.");
	#endif
	delay(10);
	imu.init();
	delay(10);

	// Set f = 100Hz
	byte acc_100hz = 0x0A;
	imu.acc.writeTo(0x2C, acc_100hz);
	// Set to +-4g range
	byte acc_4g;
	readFrom(0x31, 1, acc_4g);
	// Set to XXXX XX01
	acc_4g = acc_4g & 0xFD;
	acc_4g = acc_4g | 0x01;
	imu.acc.writeTo(0x31, acc_4g);

	// Set f = 100Hz
	imu.gyro.writemem(0x16, 0x1B);
	imu.gyro.writemem(0x15, 0x09);

	// Wait for serial port to connect
	/*while(!Serial) {}*/
	
	#ifdef DEBUG
		Serial.println("Initializing SD.");
	#endif
	if (!SD.begin(BUILTIN_SDCARD)) {
		#ifdef DEBUG
			Serial.println("SD initialization failed.");
		#endif
		_save2sd = false;
	} else {
		#ifdef DEBUG
			Serial.println("SD initialization sucessful.");
		#endif
	}

	#ifdef DEBUG
    	Serial.println("Startup sucessfull!");
	#endif

	prev_time = micros();
	over_time = 0;

	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
	display.setTextSize(1);
	display.setTextColor(WHITE);

}

void loop() {
	float meas_time;

	// Acquire values
	imu.getValues(values);
	values[0] = GRAV*(values[0] - acc_off_x) / acc_scale_x;
	values[1] = GRAV*(values[1] - acc_off_y) / acc_scale_y;
	values[2] = GRAV*(values[2] - acc_off_z) / acc_scale_z;
	values[3] = DEG2RAD*values[3];
	values[4] = DEG2RAD*values[4];
	values[5] = DEG2RAD*values[5];
	values[6] = (values[6] - magn_off_x) / magn_scale_x;
	values[7] = (values[7] - magn_off_y) / magn_scale_y;
	values[8] = (values[8] - magn_off_z) / magn_scale_z;

	meas_time = micros();

	// Time between the current and previous measure
	delta_t = (meas_time - prev_time)/SEC2MICRO;

	#ifdef DEBUG
		Serial.print("DELTA_T: ");
		Serial.println(delta_t);
		Serial.print("TStep: ");
		Serial.println(time_step);
	#endif

	if (1) {
		over_time = (delta_t - time_step);
		t += delta_t;

		#ifdef DEBUG
			Serial.print("Over time: ");
			Serial.println(over_time);
			Serial.print("Total time: ");
			Serial.println(t);
		#endif

		delta_t -= over_time;
		delayMicroseconds((time_step - delta_t)*SEC2MICRO);
		display_info();
		prev_time = meas_time;

		/*it ++;*/
	}
}
