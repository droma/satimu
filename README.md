# SatIMU

GNSS and INS integration algorithm in C/C++.

**Warning:** this project is a work in progress.

## Dependencies

   * LINUX ONLY (for the moment)
   * Boost C++ library

### Arduino

If using an arduino to acquire IMU data:

   * arduino
   * arduino10
   * arduino-mk

### Teensy

If using a teensy:

   * teensy-loader-cli
   * teensyduino

Teensy udev files must also be installed. Copy file in https://www.pjrc.com/teensy/49-teensy.rules to /etc/udev/rules.d

## Instructions

   1. git clone
   2. cd SatIMU
   3. make fresh
   4. edit conf.ini if needed (port names, different KF parameters, etc...)
   5. cd mcpu
   6. make upload
   5. ./SatIMU [-\<flag\> | --\<longflag\>] ... [-\<flag\> | --\<longflag\>]

**Note:** flag options will take precedence to the config file options.

## Troubleshooting

### Can't connect to u-blox GPS

   * Run: `ls -l /dev/tty*`
   * Find ublox port and check group
   * Add your user to group, using `gpasswd <user> -a <ublox-group>
   * Run `stty -F /dev/ttyXXX -echo`, replacing ttyXXX with your ublox port id

## To-dos

### Sensors

   * **Magnetometer measurements:** by default, the magnetometer will only be used during the setup phase, calculating the initial body heading. It's use afterwards will be optional. Reason being that the magnetometer may receive interference from other electronics, heavily hindering the navigational performance.
   * **Raw GNSS measurements:** this not be feasible since the raw data will be displayed in proprietary message protocols, which depend on the sensor model used. If necessary, only the uBlox protocol will be implemented, since it's the most common GPS manufacturer.
   * **GNSS checksum verification:** important implementation. This will discard messages that are obviously different from what the sensor intended.
   * **Calibrate accelerometer/magnetometer:** this can be done through step motors, forcing the gravitational vector & the north cardinal point to be constant in the lab frame. By rotating the sensor, all constant calibration error can be estimated.
   * **Calibrate gyroscope:** no definite method as been thought out. One possibility is to use the same step motor as the accelerometer/magnetometer calibration and rotate the system with a know angular velocity.

### Theory

   * **Linearize gravity vector**: This appears to be the most non-linear term in the ECEF kinematics model. If it can be linearized, the processing load of each filter loop will be considerably lowered.
   * **GNSS aiding**: this will send the processed position back to the GNSS sensor, aiding the initial processing.

### Code

   * **Consistent structure for vector/matrices**: either self made or Eigen
   * **Fix gravity vector model**: currently using wrong altitude
   * **Find heading through magnetometer**
   * **General organization:** separate functions in main.cpp & ekf.cpp into several helper files.
   * **Gyro spikes from startup**: may be shift problem in mcpu.
   * **Create write file mode**
   * **Fix read file mode**

### Trials

   * Create simulation testbench.
   * Test parameters in extreme cases.

